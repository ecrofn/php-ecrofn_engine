<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/ecrofn_engine/private/inc.constants.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ecrofn_engine/private/inc.messages.php';
require_once D_ENGINE_SOURCE . 'class.autoloader.php';

$autoloader = new \Ecrofn\Autoloader();

$autoloader->addNamespace('EcrofnDemo\Mvc\Controller',	D_ENGINE_ROOT . 'mvc/controller');
$autoloader->addNamespace('EcrofnDemo\Mvc\Model',		D_ENGINE_ROOT . 'mvc/model');
$autoloader->addNamespace('EcrofnDemo\Mvc\View',		D_ENGINE_ROOT . 'mvc/view');

$autoloader->register();

$application = new \Ecrofn\Application\ApplicationDevelopment(
	new \Ecrofn\Di\Container()
);
$application->run();

?>