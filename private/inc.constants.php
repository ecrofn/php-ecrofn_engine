<?php

/* Данный файл описывает константные значения используемые движком. */

/* 
* Стандарт именования и описания константы:
* Перед указанием имени константы необходимо указать преффикс ее
* использования соответствующий ее смыслу.
* 
* Легенда :
*	Расшифровка используемых префиксов
*	DB	- Database	- Работа с базой данных.
*	E	- Engine	- Внутренние данные движка.
*	LOG	- Log		- Файл с техническими записями.
*	F	- File		- Исходный файл.
*	S	- Status	- Статус выполнения операции.
*	V	- Value 	- Некое значение. 
*	ERR - Error		- Некая ошибка.
* 
*/

define('V_ACTIVE_MODULE',		'ecrofn_demo');
define('V_ROUTER_PREFIX',		'/ecrofn_engine/');
define('V_ACTIVE_ROUTE',		'EcrofnEngine_ActiveRoute');
define('V_URL_GENERATOR',		'EcrofnEngine_URLGenerator');


/* Корневая директория движка для использования файловой системой. */
define('D_ENGINE_ROOT',			$_SERVER['DOCUMENT_ROOT'] . '/ecrofn_engine/');
/* Корневая директория движка для использования WEB протоколами. */
define('D_ENGINE_ROOT_WEB',		(empty($_SERVER['HTTPS']) ? 'http' : 'https') . '://' . $_SERVER['SERVER_NAME'] . '/ecrofn_engine/');	// D_ENGINE_WEB_ROOT

define('D_ENGINE_PUBLIC',		D_ENGINE_ROOT . 'public/');
define('D_ENGINE_PUBLIC_WEB',	D_ENGINE_ROOT_WEB . 'public/');	// D_ENGINE_WEB_PUBLIC
define('D_ENGINE_PRIVATE',		D_ENGINE_ROOT . 'private/');
define('D_ENGINE_SOURCE',		D_ENGINE_ROOT . 'source/');
define('D_ENGINE_MODULES',		D_ENGINE_ROOT . 'modules/');
define('D_ENGINE_VAR',			D_ENGINE_ROOT . 'var/');

// MODULE

define('D_MODULE_ROOT',			D_ENGINE_MODULES . V_ACTIVE_MODULE . '/');
define('D_MODULE_ROOT_WEB',		D_ENGINE_ROOT_WEB . 'modules/' . V_ACTIVE_MODULE . '/');	// D_MODULE_WEB_ROOT

define('D_MODULE_PUBLIC',		D_MODULE_ROOT . 'public/');
define('D_MODULE_PUBLIC_WEB',	D_MODULE_ROOT_WEB . 'public/');	// D_MODULE_WEB_PUBLIC
define('D_MODULE_PRIVATE',		D_MODULE_ROOT . 'private/');
define('D_MODULE_SOURCE',		D_MODULE_ROOT . 'source/');
define('D_MODULE_VAR',			D_MODULE_ROOT . 'var/');

define('F_MODULE_INDEX',		D_MODULE_PUBLIC . 'index.php');	// D_MODULE_INDEX_FILE

?>