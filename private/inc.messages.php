<?php

/* Данный файл описывает сервисные сообщения используемые движком. */

/* Отсутствие предопределенного модуля. */
define('MSG_MODULE_NOT_FOUND',		'Module \'%NAME%\' not exist.');
define('MSG_CONTROLLER_NOT_FOUND',	'Controller \'%NAME%\' not exist.');
define('MSG_METHOD_NOT_FOUND',		'Method \'%NAME%\' not exist.');
define('MSG_404_NOT_FOUND',			'This is default EcrofnEngine 404 page. If you want to use your own, please add it to routes container. <br><br>Page not found.');

/* Рендер инициализирован пустыми данными. */
define('MSG_RENDER_DATA_EMPTY',			'This message means that you initialize render process, but not fill data to render.');
define('MSG_RENDERER_FILE_NOT_FOUND',	'This message means that you initialize render process, but render file not found.');

/* Работа с базами данных. */
define('MSG_DATABASE_DRIVER',		'Error while checking database connection drivers!');
define('MSG_DATABASE_CONNECTION',	'Error while connecting to database!');
define('MSG_DATABASE_TABLE',		'Error while connecting to table!');
define('MSG_DATABASE_RECORD',		'Error while adding record to table!');
define('MSG_DATABASE_CLOSE',		'Error while closing connection!');
define('MSG_DATABASE_QUR',			'Error while executing query - \'%NAME%\'!');

/*
 * Ошибки работы с БД.
 */
define('DB_E_DRV', 'Error while checking database connection drivers!');
define('DB_E_TAB', 'Error while connecting to table!');
define('DB_E_REC', 'Error while adding record to table!');
define('DB_E_CLS', 'Error while closing connection!');

define('DB_ERROR_INIT',					'Error while establishing connection with database!');
define('DB_ERROR_QUERY',				'Error while executing query!');
define('DB_NOTIFY_ALREADY_CONNECTED',	'Database connection already established!');
define('DB_EMPTY_QUERY_TEXT',			'Empty query text!');

/*
 * Доступные виды выполнения запросов.
 */
define('DB_Q_EXT', 'Inline execute');
define('DB_Q_GET', 'Get record');
define('DB_Q_ADD', 'Add record');
define('DB_Q_EDT', 'Edit record');
define('DB_Q_DEL', 'Delete record');


/*
 * Работа с файлами. 
 */
define('LOG_E_ACC', 'Error while accessing log file!');


/*
 * Служебные сообщения.
 */
define('M_TEST', 'Hello world!');

?>