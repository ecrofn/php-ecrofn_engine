<?php

/* @var $router \Ecrofn\Routing\Router */
$router = $this;

/* Adding engine generator route. */
$router->add('engen', '\EcrofnDemo\Mvc\Controller\ControllerGenerator', 'engine');
$router->add('engenwipe', '\EcrofnDemo\Mvc\Controller\ControllerGenerator', 'engine, wipe');

/* Adding module generator route. */
$router->add('modgen', '\EcrofnDemo\Mvc\Controller\ControllerGenerator');
$router->add('modgenwipe', '\EcrofnDemo\Mvc\Controller\ControllerGenerator', 'wipe');

/**
 * Sets default error route, which was prepared by EcrofnEngine.
 * This can be very useful when user doesn't defined his own error route.
*/
$router->setError('\EcrofnDemo\Mvc\Controller\ControllerMain:route_not_found', 'msg');

?>

