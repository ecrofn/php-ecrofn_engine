<?php

namespace Ecrofn\Di
{

use Ecrofn\Di\ContainerInterface;
use Ecrofn\Di\Config;

class Container implements ContainerInterface
{
	protected $config;
	
	protected $services;
	protected $shared_services;
	
	public function __construct(Config $config = null)
	{
		if ($config !== null) {
			$this->config = $config;
		}
		
		$this->services			= [];
		$this->shared_services	= [];
	}
	
	public function set($name, callable $value, $shared = false)
	{
		if ($shared) {
			$this->setShared($name, $value);
		}
		
		$this->services[$name] = $value;
	}
	
	public function setShared($name, callable $value)
	{
		$this->shared_services[$name] = $value;
	}
	
	public function setConfig(Config $config)
	{
		$this->config = $config;
		
		$this->loadConfig();
	}
	
	public function loadConfig()
	{
		
	}
	
	public function get($name, $shared = false)
	{
		if ($shared) {
			return $this->getShared($name);
		}
		
		return $this->services[$name];
	}
	
	public function getShared($name)
	{
		return $this->shared_services[$name];
	}
}

}

?>