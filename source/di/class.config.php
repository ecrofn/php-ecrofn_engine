<?php

namespace Ecrofn\Di
{

class Config extends \Ecrofn\Config\AbstractConfig
{
	public function __construct()
	{
		$this->setName('di');
	}
	
	public function set($name, callable $value)
	{
		parent::set($name, $value);
	}
	
	public function getServices()
	{
		return parent::get('services');
	}
	
	public function getSharedServices()
	{
		return parent::get('shared_services');
	}
}

}

?>