<?php

namespace Ecrofn\Di
{

use Ecrofn\Di\ContainerInterface;

interface ContainerInjectInterface
{
	/**
	 * Sets the dependency injector container.
	 * 
	 * @param \Ecrofn\Di\Container $container
	 */
	public function setDic(ContainerInterface $container);
	
	/**
	 * Gets the internal dependency injector container.
	 */
	public function getDic();
}

}

?>