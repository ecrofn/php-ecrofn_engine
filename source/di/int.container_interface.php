<?php

namespace Ecrofn\Di
{

interface ContainerInterface
{
	/**
	 * Sets the value to the key.
	 * 
	 * @param string $name
	 * @param callable $value
	 * @param bool $shared
	 */
	public function set($name, callable $value, $shared = false);
	
	/**
	 * * Sets the shared value to the key.
	 * 
	 * @param type $name
	 * @param type $value
	 */
	public function setShared($name, callable $value);
	
	/**
	 * Gets the value by it key from dependency injector container.
	 * 
	 * @param string $name
	 */
	public function get($name);
	
	/**
	 * Gets the shared value by it key from dependency injector container.
	 * 
	 * @param string $name
	 */
	public function getShared($name);	
}

}

?>