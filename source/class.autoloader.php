<?php

// http://www.php-fig.org/psr/psr-4/
// http://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
// http://victor.4devs.io/ru/php/classes-autoload-php.html

namespace Ecrofn
{

class Autoloader
{
	const FRAMEWORK_VENDOR = 'Ecrofn';
	
	protected $prefixes		= array();
	protected $extensions	= array();
	
	public function __construct()
	{
		$this->extensions[] = 'class.';	/* Class */
		$this->extensions[] = 'int.';	/* Interface */
		$this->extensions[] = 'trt.';	/* Trait	*/
	}
	
	public function register()
	{
		spl_autoload_register(array($this, 'loadClass'));
	}
	
	public function loadClass($class)
	{
		$result = $this->loadClassDirect($class);
		
		if ($result == false) {
			$result = $this->loadClassSmart($class);
		}
		
		return $result;
	}
	
	public function loadClassDirect($class)
	{
		$splitted_class = $this->splitRelativeClassName($class, false);
		
		$prefix = $splitted_class['vendor'] . $splitted_class['namespace'];
		
		if (isset($this->prefixes[$prefix]) === false) {
			return false;
		}
		
		/* Make correct filename (class) representation. */
		$original_class = $this->generateFileName($splitted_class['class']) . '.php';
		
		$files		= array();
		$files[]	= $original_class;
		foreach ($this->extensions as $extension) {
			$files[] = $extension . $original_class;
		}
		
		foreach ($this->prefixes[$prefix] as $base_dir) {
			
			foreach ($files as $file_name) {
				$file = $base_dir . str_replace('\\', '/', $file_name);
				
				if ($this->requireFile($file)) {
					return $file;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Loads the class file for a given class name.
	 *
	 * @param string $class The fully-qualified class name.
	 * @return mixed The mapped file name on success, or boolean false on
	 * failure.
	 */
	public function loadClassSmart($class)
	{
		$splitted_class = $this->splitRelativeClassName($class, true);
		
		/* Make correct vendor representation. */
		$vendor_directory = '';		
		if ($splitted_class['vendor'] == self::FRAMEWORK_VENDOR) {
			$vendor_directory = D_ENGINE_SOURCE;
		}
		else {
			$vendor_directory = D_ENGINE_MODULES . $this->generateFileName($splitted_class['vendor']) . '/source/';
		}
		
		/* Make correct namespace representation for the correct work with file system. */
		$namespace = str_replace('\\', '/', $this->generateFileName($splitted_class['namespace'])) . '/';
		
		/* Make correct filename (class) representation. */
		$original_class = $this->generateFileName($splitted_class['class']) . '.php';
		
		$files		= array();
		$files[]	= $original_class;
		foreach ($this->extensions as $extension) {
			$files[] = $extension . $original_class;
		}	
		
		
		/* Searching process. */
		foreach ($files as $file) {
			$file_name = $vendor_directory . $namespace . $file;
			
			if ($this->requireFile($file_name)) {
				return $file;
			}
		}
		
		return false;
	}
	
	public function addNamespace($prefix, $base_dir, $prepend = false)
	{
		$prefix = trim($prefix, '\\') . '\\';
		
		$base_dir = rtrim($base_dir, DIRECTORY_SEPARATOR) . '/';
		
		// initialize the namespace prefix array
		if (isset($this->prefixes[$prefix]) === false) {
			$this->prefixes[$prefix] = array();
		}
		
		if ($prepend) {
			array_unshift($this->prefixes[$prefix], $base_dir);
		} else {
			array_push($this->prefixes[$prefix], $base_dir);
		}
	}
	
	/**
	 * If a file exists, require it from the file system.
	 * 
	 * @param string $file The file to require.
	 * @return bool True if the file exists, false if not.
	 */
	protected function requireFile($file)
	{
		if (file_exists($file)) {
			require $file;
			return true;
		}
		return false;
	}
	
	/**
	 * Forms file name, which corresponds to the engine pattern. 
	 * 
	 * @param string $className
	 * @return string
	 */
	private function generateFileName($className)
	{
		$length		= strlen($className);
		$result		= '';
		
		$ignored		= array('\\', '/');
		$was_ignored	= false;
		
		for ($counter = 0; $counter < $length; $counter++) {
			
			$symbol = ord($className[$counter]);
			
			/* Check symbol in A-Z range. */
			if (($symbol >= 65) && ($symbol <= 90)) {
				
				/* First symbol or underline mode was ignored. */
				if (($counter == 0) || ($was_ignored)) {
					$result .= (chr($symbol + 32));
				}
				/* Complete result with underline symbol. */
				else {
					$result .= ('_' . chr($symbol + 32));
				}
			}
			else {
				$result .= chr($symbol);
				$was_ignored = array_search(chr($symbol), $ignored, true) !== false ? true : false;
			}
		}
		
		return $result;
	}
	
	/**
	 * Splits input value by vendor, namespace, class values.
	 * 
	 * @param string $name The name to be splitted.
	 * @param bool $exclude_trailing_slash
	 * @return array An array of splitted strings.
	 */
	private function splitRelativeClassName($name, $exclude_trailing_slash = false)
	{
		$result = array(
			'vendor'	=> '',
			'namespace'	=> '',
			'class'		=> $name
		);
		
		$lpos = strpos($name, '\\');
		$rpos = strrpos($name, '\\');
		
		if (($lpos && $rpos) !== false) {
			
			$offset_1 = ($exclude_trailing_slash) ? 0 : 1;
			$offset_2 = ($exclude_trailing_slash) ? 1 : 0;
			
			$result['vendor']		= substr($name, 0, $lpos + $offset_1);
			/**
			 * If lpos & rpos are same value - there is nothing to calculate,
			 * result will be always empty string.
			 */
			$result['namespace']	= ($lpos == $rpos)
										? ''
										: substr($name, $lpos + 1, $rpos - ($lpos + $offset_2));
			$result['class']		= substr($name, $rpos + 1);
		}
		
		return $result;
	}
}

}

?>