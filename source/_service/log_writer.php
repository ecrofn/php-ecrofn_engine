<?php

/*
 * Данный класс отвечает за работу с файлами логов, используя
 * шаблон "Одиночка" (Singleton pattern)
 * 
 * @author Ecrofn
 */
class LogWriter
{
	
	private $path;
	
	public function __construct()
	{
		$path = PROJECT_ROOT . 'logs/';
	}
	
    public function WriteLog($status = null, $job = null, $description = null)
    {	
		// echo('LW : is on repair! <br></br>');
		return false;
		
		if ($status == null) {
			$status = 1;
		}
		if ($job == null) {
			$job = '';
		}
		if ($description == null) {
			$description = '';
		}

		$folder_name = PROJECT_ROOT . date('M') . '_' . date('Y');	// Jan_1999, Feb_2015, ...
		
		if (!file_exists($folder_name))
		{
			mkdir($folder_name);
		}
			
		$separator			= '		';  // 2 tab x 4 symbols
		$begin_of_the_day 	= mktime(0, 0, 0);
		$file_name			= $folder_name . '/' . strval($begin_of_the_day) . '.log';
		$time				= date('H:i:s');    // Текущее время
		
		$CREATE_IF_NOT_EXIST_READ_ONLY	= 'a';

		// Полученная строка
		$result_string 	= $time . $separator . $status . $separator .
						$job . $separator . $description . chr(13) . chr(10);											

		$fp = fopen($file_name, $CREATE_IF_NOT_EXIST_READ_ONLY);
		if ($fp)
		{
			fwrite($fp, $result_string);
			fclose($fp);
		}
    }
    
}
	
?>