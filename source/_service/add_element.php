<?php
	
	include 'database_work.php';		

	
	//echo(preg_match("/^.*\.(?:jpg|jpeg|png)\s*$/i", "name.png"));
	//CheckFields();
	
	DrawHTMLCode();
	
	if (CheckFields())
	{
		//$connection = CreateConnection();
		
		//if (!CheckItemExists($connection))
		//{
		//	AddRecord($connection);
		//	MoveUploadedFiles();
		//}
		
		//CloseConnection($connection);
	}
	else
		echo('incorrect');
	

	
	/** ** ** ** РАЗДЕЛ ОПИСАНИЯ МЕТОДОВ ** ** ** **/ /** ** ** ** РАЗДЕЛ ОПИСАНИЯ МЕТОДОВ ** ** ** **/
	
	function DrawHTMLCode()
	{
		echo("
		<html
			<head>
				<meta http-equiv = 'content-type' content = 'text/html' charset = 'utf-8'> 
				<script type = 'text/javascript' src = 'js/work_helper.js'></script>
		
			</head>
			<title> Add record to DB </title>
			<body>	
				<form name = 'form' action = 'add_element.php' method = 'POST' enctype = 'multipart/form-data' onSubmit = 'return not CheckFields();'>
			
				<select
					name = 'ItemType'	<! Здесь должен быть запрос к таблице с группами, на основании которой генерируются записи>
					id = 'ItemType'>
					<option>one</option>
					<option>two</option>
				</select>
				<br>
				<input type = 'text' name = 'ItemName' id = 'ItemName' class = 'stringElements'>
				<br>
				<textarea rows = '3' cols = '40' name = 'Description' placeholder = 'Input text here...' class = 'stringElements'></textarea>
				<br>
				<input type = 'file' name = 'Images' id = 'Images' multiple />
				<br>
				<input type = 'text' name = 'Quantity' id = 'Quantity class = 'integerElements'> 
				<br>
				<input type = 'text' name = 'Price' id = 'Price' class = 'integerElements'>
				
				
				<br>
				<input type = 'submit' id = 'SubmitButton' value = 'Insert record'>
			
				</form>	
			</body>
		</html>
		");
	}
	
	function CheckPOSTParams()
	{
		// Обращение к скрипту выполнено не через форму посредника, а простым
		// копированием URL'а. Это подразумевает что на скрипт оказывает
		// воздействие посторонний фактор (взлом, брут, и т.д.)
		//return count($_POST) == 0;
		echo(count($_POST));
	}
	
	function CreateConnection()
	{
		$db_work = new DBWork();
		$db_work->ConnectToServer("localhost", "root", "");
		$db_work->ConnectToDataBase("intshop");
		
		return $db_work;		
	}
	
	function CloseConnection($connection)
	{
		$connection->CloseConnection();
	}
	
	function AddRecord($connection)
	{
		$query =	"INSERT INTO Items(" .
						"ItemName," .
						"Description," .
						"Photo," .
						"Quantity," .
						"Price)" .
					"VALUES(" .
						"'" . $_POST['ItemName'] . "'," .
						"'" . $_POST['Description'] . "'," .
						"'" . GenerateImagesPaths() . "'," .
						$_POST['Quantity'] . "," .
						$_POST['Price'] . ")";
		
		$connection->ExecuteQuery($query);
	}
	
	function CheckItemExists($connection)
	{
		// Проверка на дубли, и нужная логика
		$query =	"SELECT 
						ItemName
					FROM Items
					WHERE
						ItemName = '" . $_POST['ItemName'] . "'";
		$result = $connection->ExecuteQuery($query);
		$count = mysql_num_rows($result);
		
		return ($count > 0);
		
	}
	// Проверка на заполненность полей и корректность входных данных
	function CheckFields()
	{		
		$EMPTY_STRING = '';
		$AllOk = true;		
		$AllowedVarTypes = array(
							'/(^[A-z0-9 ]*)$/',	// Любая буква, цифра и символ пробела
							'/(^[A-z0-9 ]*)$/',	// Любая буква, цифра и символ пробела
							'/(^[A-z0-9 ]*)$/',	// Любая буква, цифра и символ пробела
							'/^\d+$/',			// Любая цифра
							'/^\d+$/');			// Любая цифра				
		$ErrorMessages = $EMPTY_STRING;
		
		$counter = 0;
				
		foreach($_POST as $name => $value)
		{
			// Проверка текущего поля на пустое значение
			if (empty($value))
			{
				$ErrorMessages = $ErrorMessages . 'Field - ' . $name . ' is not filled! <br>';
			}
			// Проверка текущего поля на соответствие шаблону типа
			if (!preg_match($AllowedVarTypes[$counter], $value))
			{
				$ErrorMessages = $ErrorMessages . 'Field - ' . $name . ' has wrong type <br>';
			}
			$counter++;
		}
		
		// Проверка на наличие ошибок
		if ($ErrorMessages != $EMPTY_STRING)
		{
			$ErrorFile = file_get_contents('./error_page.html');
			$ErrorFile = str_replace('[ERROR_DESCRIPTION]', $ErrorMessages, $ErrorFile);
			//$ErrorFile = str_replace('[RETURN_LINK]', ' <a href="../../example/knob.html">Относительная ссылка</a>', $ErrorFile);
			$AllOk = false;
			echo($ErrorFile);
		}
		
		return $AllOk;
		
	}
	
	// Перенести файлы из временной папки в сгенерированную
	function MoveUploadedFiles()
	{
		$dirName = GetDirName($_POST['ItemName']);
		$files = $_FILES['Images'];
		
		foreach ($files['tmp_name'] as $index => $value)
		{
			move_uploaded_file($files['tmp_name'][$index],
						$dirName . $files['name'][$index]);
		}			
	}
	
	// Метод на основании названия товара генерирует пути к изображениям на сервере
	function GenerateImagesPaths()
	{
		$Images = $_FILES['Images'];
		$dirPath = GetDirName($_POST['ItemName']);
		$imagesPath = '';
	
		// Проверка параметра на заполненность данными.
		// Достаточно проверить первый элемент массива.
		if (empty($Images['name'][0]))
			// Параметр пустой, в качестве изображения инициализируется
			// стандартная картина
			$imagesPath = 'images/no_photo_available.png';
		else
			// Параметр заполненный, в качестве изображений инициализируются
			// картинки указанные пользователем
			foreach ($Images['name'] as $currentImage)
			{
				$imagesPath = $imagesPath . $dirPath . $currentImage . chr(13);
			}

		return($imagesPath);
	}
	
	// Устанавливает имена для загруженных изображений
	function SetImageName()
	{
	}
	
	// На основании имени товара будет сгенериован путь к папке, содержащей
	// нужные данные
	function GetDirName($itemName)
	{
		// Заменить все пробелы на знак нижнего подчеркивания
		$itemName = str_replace(chr(32), '_', $itemName);
		$path = 'images/' . strtolower($itemName) . '/';
		
		// Создать папку, если такой нету
		if (!file_exists($path))
			mkdir($path);
		
		return $path;
	}
	
?>
