<?php

namespace Ecrofn\Application
{

use Ecrofn\Di\ContainerInjectInterface;
use Ecrofn\Di\ContainerInterface;

abstract class AbstractApplication implements ContainerInjectInterface
{
	protected static $modes = array(
		'DEVELOPMENT',
		'PRODUCTION'
	);
	
	private $router = null;
	
	/** @var Ecrofn\Di\ContainerInterface Description */
	protected $dic;
	
	public abstract function getMode();
	
	public function __construct(ContainerInterface $container)
	{
		$this->setDic($container);
		$this->initializeRouter();
		//$this->initializeDependecies();
		
		$container->loadConfig();
	}
	
	public function setDic(ContainerInterface $container)
	{
		$this->dic = $container;
	}
	
	public function getDic()
	{
		return $this->dic;
	}
	
	public function run()
	{
		$this->router->handle();
	}
	
	protected function initializeRouter()
	{
		$this->router = new \Ecrofn\Routing\Router();
		
		$this->router->setRoutePrefix(V_ROUTER_PREFIX);
		//$this->router->setRequest(new \Ecrofn\Routing\Request);
	}
	
	protected function initializeDependecies()
	{
		$this->dic->setShared('Session',
			function() {
				return new \Ecrofn\Storage\Session();
			}
		);
		
		$this->dic->setShared('Request',
			function() {
				return new \Ecrofn\Routing\Request();
			}
		);
	}
}

}

?>
