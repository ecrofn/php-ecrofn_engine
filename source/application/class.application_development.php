<?php

namespace Ecrofn\Application
{

use Ecrofn\Application\AbstractApplication;

final class ApplicationDevelopment extends AbstractApplication
{
	private $_code_analyzer;
	
	public function getMode()
	{
		return self::$modes[0];
	}
	
	protected function initializeRequiredObjects()
	{
		//parent::initializeRequiredObjects();
	}
	
	private function initializeCodeAnalyzer()
	{
		if ($this->_code_analyzer == null)
		{
			// Lazy load.
			require D_ENGINE_CORE . 'system' . DIR_SEPARATOR . 'debug' . DIR_SEPARATOR . 'class.code_analyzer.php';
		}
	}
	
	private function requireErrorReporting()
	{
		/**
		* Configuration for: Error reporting
		* Useful to show every little problem during development, but only show hard errors in production
		*/
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}
}

}

?>