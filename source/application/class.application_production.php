<?php

namespace Ecrofn\Application
{

use Ecrofn\Application\AbstractApplication;

final class ApplicationProduction extends AbstractApplication
{
	public function getMode()
	{
		return self::$modes[1];
	}
}

}

?>