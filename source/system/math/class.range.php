<?php

namespace Ecrofn\System\Math
{

class Range
{
	public static function inRange($value, $startIndex, $endIndex)
	{
		return (($value >= $startIndex) && ($value <= $endIndex));
	}
	
	public static function normalizeLapRange($value, $minRange, $maxRange)
	{
		$_value = $value;
		
		if ($value < $minRange) {
			$_value = $maxRange;
		} elseif ($value > $maxRange) {
			$_value = $minRange;
		}
		
		return $_value;
	}
	
	public static function normalizeBorderRange($value, $minRange, $maxRange)
	{
		$_value = $value;
		
		if ($value < $minRange) {
			$_value = $minRange;
		} elseif ($value > $maxRange) {
			$_value = $maxRange;
		}
		
		return $_value;
	}
}

}

?>