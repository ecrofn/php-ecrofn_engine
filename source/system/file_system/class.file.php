<?php

namespace Ecrofn\System\FileSystem
{

use Ecrofn\System\FileSystem\AbstractEventableFile;
use Ecrofn\System\FileSystem\Exception;
use Ecrofn\System\FileSystem\Directory;

class File extends AbstractEventableFile
{
	public static function exists($path)
	{
		return file_exists($path) && is_file($path);
	}
	
	public static function isReadable($path)
	{
		return self::exists($path) && is_readable($path);
	}
	
	public static function isWritable($path = true)
	{
		return self::exists($path) && is_writable($path);
	}
	
	public function __construct($path)
	{
		$this->prepare($path);
	}
	
	public function getSize()
	{
		if (!self::exists($this->path)) {
			return 0;
		}
		$size = filesize($this->getPath());
		
		$this->size = $size;
		
		return $size;
	}
	
	public function create($content, $mode = 0777)
	{
		$this->beforeCreate();
		
		if (!Directory::exists($this->parent_path)) {
			new Directory($this->parent_path, true);
		}
		
		if (self::exists($this->path)) {
			throw new Exception("File $this->path already created!");
		}
		
		$this->setContent($content);
		
		if (!chmod($this->path, $mode)) {
			chmod($this->path, 0777);
		}
		
		$this->afterCreate();
	}
	
	public function delete()
	{
		$this->removeFile($this->path);
	}
	
	public function setContent($content)
	{
		if (file_put_contents($this->path, $content) === false) {
			throw new Exception("Unable to set content to file $this->path!");
		}
	}
	
	public function getContent()
	{
		$content = file_get_contents($this->path);
		
		if ($content === false) {
			throw new Exception("Unable to read file $this->path!");
		}
		
		return $content;
	}
	
	protected function prepare($path)
	{
		parent::prepare($path);
		
		/*
		 * Normalize file path with removing the trailing slash.
		 * 
		 * This will help to determine that this is file and not
		 * a directory with dots in name.
		 * 
		 * http://stackoverflow.com/a/980464
		 */
		$this->path			= rtrim($this->path, '/');
		$extension			= strrchr($this->path, '.');
		$this->extension	= $extension ? $extension : '';
	}
	
	protected function removeFile($path)
	{
		$this->beforeDelete();
		
		if (!unlink($path)) {
			throw new Exception("Unable to delete file $path!");
		}
		
		$this->afterDelete();
	}
}

}

?>