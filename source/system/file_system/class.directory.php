<?php

namespace Ecrofn\System\FileSystem
{

use Ecrofn\System\FileSystem\AbstractEventableDirectory;
use Ecrofn\System\FileSystem\Exception;
use Ecrofn\System\FileSystem\File;

class Directory extends AbstractEventableDirectory
{
	private $ignored_files;
	private $handle = null;
	
	public static function exists($path)
	{
		return file_exists($path) && is_dir($path);
	}
	
	public function __construct($path, $create = false)
	{
		$this->prepare($path);
		
		if ($create) {
			$this->create(0777, true);
		}
	}
	
	public function getSize()
	{
		if (!self::exists($this->path)) {
			return 0;
		}
		
		$total_size	= 0;
		$files		= $this->getFiles();
		
		foreach ($files as $file) {
			if ($file->isDirectory()) {
				$size = $file->getSize();
				$total_size += $size;
			} else {
				$size = filesize($file->getPath());
				$total_size += $size;
				unset($file);
			}
		}
		$this->size = $total_size;
		
		return $total_size;
	}
	
	public function create($mode = 0777, $recursive = false)
	{
		$this->beforeCreate();
		
		$result = mkdir($this->path, $mode, $recursive);
		
		if (!$result) {
			throw new Exception("Unable to create dir $this->path!");
		}
		
		$this->afterCreate();
	}
	
	public function clean($ignored = array())
	{
		$this->cleanRecursive($this->path, array_merge(
				$this->ignored_files, $ignored)
		);
	}
	
	public function delete()
	{
		$this->cleanRecursive($this->path, $this->ignored_files);
		$this->removeDirectory($this->path);
	}
	
	public function getFiles($type = parent::FILE_TYPE_ALL, $ignored = array())
	{
		if (!self::exists($this->path)) {
			throw new Exception('Directory does not exist!');
		}
		
		$_ignored = array_merge($this->ignored_files, $ignored);
		
		$result = array();
		$files = scandir($this->path);
		
		foreach ($files as $file) {
			if (!in_array($file, $_ignored)) {
				
				$file_path = $this->path . $file;
				
				if (is_dir($file_path)) {
					if ($type & parent::FILE_TYPE_DIRECTORY) {
						$result[] = new Directory($file_path);
					}
				} else {
					if ($type & parent::FILE_TYPE_FILE) {
						$result[] = new File($file_path);
					}
				}
			}
		}
		
		return $result;
	}
	
	protected function prepare($path)
	{
		parent::prepare($path);
		
		/*
		 * Normalize directory path with trailing slash.
		 * 
		 * Trailing slash helps to determine that this is directory and
		 * not file without extension.
		 * 
		 * http://stackoverflow.com/a/980464
		 */
		$this->path				= rtrim($this->path, '/') . '/';
		
		$this->ignored_files	= array('.', '..');
	}
	
	protected function removeDirectory($path)
	{
		$this->beforeDelete();
		
		if (!rmdir($path)) {
			throw new Exception("Unable to delete dir $path!");
		}
		
		$this->afterDelete();
	}
	
	private function cleanRecursive($path, $ignored = array())
	{
		if (!self::exists($path)) {
			return;
		}
		
		$files = $this->getFiles(parent::FILE_TYPE_ALL, $ignored);
		
		foreach ($files as $file) {
			if ($file->isDirectory()) {
				$file->cleanRecursive($file->getPath(), $ignored);
				$file->removeDirectory($file->getPath());
			} else {
				$file->delete($file->getPath());
			}
		}
	}
}

}

?>