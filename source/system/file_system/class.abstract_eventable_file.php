<?php

namespace Ecrofn\System\FileSystem
{

use Ecrofn\System\FileSystem\AbstractFile;

use Ecrofn\Event\EventableInterface;
use Ecrofn\Event\EventManagerInterface;

abstract class AbstractEventableFile extends AbstractFile
	implements EventableInterface
{
	private $em;
	
	public function setEventsManager(EventManagerInterface $event_manager)
	{
		$this->em = $event_manager;
	}
	
	public function getEventsManager()
	{
		return $this->em;
	}
	
	protected function beforeCreate()
	{
		if ($this->em) {
			$this->em->fireEvent('beforeCreate', $this);
		}
	}
	
	protected function afterCreate()
	{
		if ($this->em) {
			$this->em->fireEvent('afterCreate', $this);
		}
	}
	
	protected function beforeDelete()
	{
		if ($this->em) {
			$this->em->fireEvent('beforeDelete', $this);
		}
	}
	
	protected function afterDelete()
	{
		if ($this->em) {
			$this->em->fireEvent('afterDelete', $this);
		}
	}
}

}

?>