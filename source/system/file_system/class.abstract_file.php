<?php

namespace Ecrofn\System\FileSystem
{

abstract class AbstractFile
{
	const FILE_TYPE_FILE		= 1;
	const FILE_TYPE_DIRECTORY	= 2;
	const FILE_TYPE_ALL			= 3;
	
	const INFORMATION_PATH		= 1;	// 0000	0001
	const INFORMATION_PPATH		= 2;	// 0000	0010
	const INFORMATION_NAME		= 4;	// 0000	0100
	const INFORMATION_EXTENSION	= 8;	// 0000	1000
	const INFORMATION_SIZE		= 16;	// 0001	0000
	const INFORMATION_ALL		= 31;	// 0001	1111
	
	protected $path;
	protected $parent_path;
	protected $name;
	protected $size;
	protected $extension;
	
	abstract public static function exists($path);
	
	abstract public function __construct($path);
	
	abstract public function getSize();
	
	/**
	 * Checks current entity for matching Directory type.
	 * 
	 * @return bool TRUE if current entity matches Directory type, otherwise
	 *	FALSE.
	 */
	public function isDirectory()
	{
		return is_dir($this->path);
	}
	
	/**
	 * Checks current entity for matching File type.
	 * 
	 * @return bool TRUE if current entity matches Directory type, otherwise
	 *	FALSE.
	 */
	public function isFile()
	{
		return is_file($this->path);
	}
	
	/**
	 * Gets a full path of the entity.
	 * 
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}
	
	/**
	 * Gets a parent path of the entity.
	 * 
	 * @return string
	 */
	public function getParentPath()
	{
		return $this->parent_path;
	}
	
	/**
	 * Gets an entity name.
	 * 
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Gets an entity extension.
	 * 
	 * @return string
	 */
	public function getExtension()
	{
		return $this->extension;
	}
	
	/**
	 * Gets a general information about entity.
	 * 
	 * @param type $flags
	 * 
	 * @return type
	 */
	public function getInformation($flags = self::INFORMATION_ALL)
	{
		$information = array();
		
		if ($flags & self::INFORMATION_PATH)		{ $information['path']		= $this->getPath(); }
		if ($flags & self::INFORMATION_PPATH)		{ $information['ppath']		= $this->getParentPath(); }
		if ($flags & self::INFORMATION_NAME)		{ $information['name']		= $this->getName(); }
		if ($flags & self::INFORMATION_EXTENSION)	{ $information['extension']	= $this->getExtension(); }
		if ($flags & self::INFORMATION_SIZE)		{ $information['size']		= $this->getSize(); }
		
		return $information;
	}
	
	/**
	 * 
	 * @param type $path
	 */
	protected function prepare($path)
	{
		/*
		 * According to the http://stackoverflow.com/a/4178279
		 * convert all directory separators to *nix format because
		 * Windows OS could work with both of separators type.
		 */
		
		// Path.
		$this->path = str_replace('\\', '/', $path);
		// Parent path.
		$this->parent_path = str_replace('\\', '/', dirname($this->path));
		// Name.
		$this->name = basename($this->path);
		// Size.
		$this->size = 0;
		// Extension.
		$this->extension = '';
	}
}

}

?>