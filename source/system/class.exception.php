<?php

namespace Ecrofn\System
{

use Exception as PHPException;
use ErrorException as PHPErrorException;

class Exception extends PHPException
{
	protected $code = 0;
	
	public function __construct($message = '', $code = 0)
	{
		parent::__construct($message, $code);
		
		//if ($write_logs)
		//{
			//$this->writeLog();
		//}
    }
	
	public function __toString()
	{
		return parent::__toString();
	}
	
	private function writeLog()
	{
		echo('THIS BLOCK IS MUST BE REPLACED WITH LOG-WRITTER CLASS');
	}
}

class WarningException				extends PHPErrorException {}
class ParseException				extends PHPErrorException {}
class NoticeException				extends PHPErrorException {}
class CoreErrorException			extends PHPErrorException {}
class CoreWarningException			extends PHPErrorException {}
class CompileErrorException			extends PHPErrorException {}
class CompileWarningException		extends PHPErrorException {}
class UserErrorException			extends PHPErrorException {}
class UserWarningException			extends PHPErrorException {}
class UserNoticeException			extends PHPErrorException {}
class StrictException				extends PHPErrorException {}
class RecoverableErrorException		extends PHPErrorException {}
class DeprecatedException			extends PHPErrorException {}
class UserDeprecatedException		extends PHPErrorException {}

set_error_handler(function ($err_severity, $err_msg, $err_file, $err_line, array $err_context)
{	
	// error was suppressed with the @-operator
	// if (0 === error_reporting()) { return false; }
	switch($err_severity)
	{
		case E_ERROR:				throw new ErrorException			($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_WARNING:				throw new WarningException			($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_PARSE:				throw new ParseException			($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_NOTICE:              throw new NoticeException			($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_CORE_ERROR:			throw new CoreErrorException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_CORE_WARNING:		throw new CoreWarningException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_COMPILE_ERROR:		throw new CompileErrorException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_COMPILE_WARNING:		throw new CoreWarningException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_USER_ERROR:			throw new UserErrorException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_USER_WARNING:		throw new UserWarningException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_USER_NOTICE:			throw new UserNoticeException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_STRICT:				throw new StrictException			($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_RECOVERABLE_ERROR:	throw new RecoverableErrorException	($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_DEPRECATED:			throw new DeprecatedException		($err_msg, 0, $err_severity, $err_file, $err_line);
		case E_USER_DEPRECATED:		throw new UserDeprecatedException	($err_msg, 0, $err_severity, $err_file, $err_line);
	}
});

}

?>
