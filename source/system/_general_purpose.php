<?php

/*
 * -----------------------------------------------------------------------------
 * Данный класс содержит в себе список всех часто используемых методов.
 * 
 * @author Ecrofn
 * -----------------------------------------------------------------------------
 */

class GeneralPurpose
{
	public function MaintainFile($file_name, $job)
	{
		$fp = fopen($file_name, $job);
		if ($fp)
		{
			fwrite($fp, 'some data');
			fclose($fp);
			// Log writer
			// LW->Write('test');
		}
		//else
			//LW->Write('error');
	}

	// Метод выводит список в удобном для пользователя виде
	public function PrintList($data, $separator, &$result = '')
	{		
		if ($this->CheckVariableType($data, 'array'))
		{
			/*foreach ($data as $sub_data)						
			{
				if ($this->CheckVariableType($sub_data, 'array'))
				{
					$this->PrintList($sub_data, chr(9) . $separator, $result);
				}
				else
				{
					$result .= $sub_data . $separator;
				}
			}*/
		 print_r($data);
		}
		else
		{
			$result .= $data . $separator;
		}
		echo ($result);
	}
	
	/*
	 * Метод проверяет тип переменной. 
	 */
	public static function CheckVariableType($variable, $type)
	{
		return gettype($variable) == $type ? true : false;
	}

	/*
	 * Метод удаляет из массива указанные элементы.
	 * @param array $array
	 * Исходный массив.
	 * @param string
	 * $indexes Индексы массива которые необходимо удалить.
	 * В качестве разделителя используется символ запятой (,), без пробелов!
	 * Внимание!
	 *	Нумерация индексов начинается с 0.
	 *	Нет необходимости вычислять смещение индекса для вновь удаляемого
	 *	элемента относительно предыдущего, метод реализует это действие сам.
	 * @return array Обновленный массив с учетом смещений.
	 * 
	 */
	public function CutArray($aSourceArray, $sIndexes)
	{
		$indexes	= explode(',', $sIndexes);
		// Преобразовать все значения в целочисленный тип.
		$aIndexes	= array_map(function($value){ return intval($value); }, $sIndexes);
		$offset		= 0;
		
		foreach ($aIndexes as $value) 
		{			
			array_splice($aSourceArray, $value - $offset, 1);
			$offset++;
		}
		return $aSourceArray;
	}
	
	// 
	public function ShowMessage($text, $separator = '')
	{
		// Найти все переносы строк.
		$strings = explode(chr(13) + chr(10), $text);
		
		foreach ($strings as $currentString)
		{					
			printf('%s', $currentString . $separator);
		}				
	}
	
	// Метод приостанавливает выполнение текущей операции
	// и выводит сообщение об ошибке.
	//
	// В СТАДИИ ПРОЕКТИРОВАНИЯ!!!
	private function SuspendExecute($error_code)
	{
		exit($error_code);
	}	
}

?>