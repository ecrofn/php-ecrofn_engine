<?php

namespace Ecrofn\System\Structure
{

use Ecrofn\System\Structure\Exception;

class Struct
{
	const DEFAULT_KEY_VALUE = null;
	
	/** @var Array Internal container for all structure keys & values. */
	protected $_container		= array();
	protected $_reserved_keys	= array();
	
	/**
	 * Creates a new structure from input data. The structure object represents
	 * itself as object, containing the pairs of key & value.
	 * 
	 * The keys values could be assigned by two different ways:
	 * 1. By using the standard method via multiple variable creation.
	 *	<b>$foo = new Structure('bar, baz', $baz, $baz);</b>
	 * 2. By using the array creation method.
	 *	<b>$foo = new Structure('bar, baz', array($baz, $baz));</b>
	 *	An array creation method works when only the input array are passed and
	 *	nothing else. If another variables are passed too, then this array are
	 *	just attaches to the corresponding key and becomes it value.
	 * 
	 * @param string $keysNames The list of keys names separated by ',' symbol.
	 * The empty keys names are ignored.
	 * @param mixed $keysValues [optional] The list of keys values.
	 *	The keys values are aligning by the keys names. If count of the keys
	 *	values are bigger than the keys names, the rest of the keys values are
	 *	ignored. Otherwise if count of the keys values are smaller than the keys
	 *	names, the rest of the missing keys values are filling up with
	 *  predefined variable DEFAULT_KEY_VALUE.
	 * @param mixed $_ [optional]
	 * 
	 * @throws \Ecrofn\System\Structure\Exception
	 */
	public function __construct($keysNames, $keysValues = null, $_ = null)
	{
		$this->fillReservedKeysNames();
		
		$keysValues = func_get_args();
		array_shift($keysValues);
		
		$nv = $this->prepareKeysNamesAndValues($keysNames, $keysValues);
		$this->fillKeysAndValues($nv['names'], $nv['values']);
	}
	
	public function __get($keyName)
	{
		return $this->get($keyName);
	}
	
	public function __set($keyName, $keyValue)
	{
		$this->set($keyName, $keyValue);
	}
	
	/**
	 * Gets value of the required key. Alias of __get method.
	 * Could be useful when the name of the key are located in the variable.
	 * 
	 * @param string $keyName They key name, which value should be retrieved.
	 * 
	 * @return mixed The required value which corresponds the required key.
	 * 
	 * @throws \Ecrofn\System\Structure\Exception
	 */
	public function get($keyName)
	{
		if (!$this->has($keyName)) {
			throw new Exception("Get operation. Key '$keyName' not exist.");
		} else {
			return $this->_container[$keyName];
		}
	}
	
	/**
	 * Sets value to the required key. Alias of __set method.
	 * Could be useful when the name of the key are located in the variable.
	 * 
	 * @param string $keyName The key name, which value should be set.
	 * @param mixed $keyValue The key value to set.
	 * 
	 * @throws \Ecrofn\System\Structure\Exception
	 */
	public function set($keyName, $keyValue)
	{
		if ($this->has($keyName)) {
			$this->_container[$keyName]	= $keyValue;
			$this->$keyName				= $keyValue;
		} else {
			throw new Exception("Set operation. Key '$keyName' not exist.");
		}
	}
	
	/**
	 * Verifies existence of the key.
	 * 
	 * @param string $keyName The key name to be verified.
	 * 
	 * @return bool TRUE, if key exist, otherwise FALSE.
	 */
	public function has($keyName)
	{
		return array_key_exists($keyName, $this->_container);
	}
	
	/**
	 * Assigns selected value to all object keys.
	 * 
	 * @param mixed $value Value to be assigned.
	 */
	public function setCommonKeysValue($value)
	{
		foreach ($this->_container as $key_name => $key_value) {
			$key_value = $value;
			$this->set($key_name, $key_value);
		}
	}
	
	/**
	 * Assigns selected value to all empty object keys.
	 * 
	 * @param mixed $value Value to be assigned.
	 */
	public function setEmptyKeysValue($value)
	{
		foreach ($this->_container as $key_name => $key_value) {
			if ($key_value == null) {
				$key_value = $value;
				$this->set($key_name, $key_value);
			}
		}
	}
	
	/**
	 * Gets count of the keys.
	 * 
	 * @return int The count of keys.
	 */
	public function count()
	{
		return count($this->_container);
	}
	
	/**
	 * Verifies existence of the keys.
	 * 
	 * @return bool TRUE, if at least one key exist, otherwise FALSE.
	 */
	public function isEmpty()
	{
		return ($this->count() == 0);
	}
	
	/**
	 * Adds a new key to the structure.
	 * 
	 * @param string $keyName The key name to be added.
	 * @param mixed $keyValue The key value.
	 * 
	 * @throws \Ecrofn\System\Structure\Exception
	 */
	protected function add($keyName, $keyValue = self::DEFAULT_KEY_VALUE)
	{
		if ($this->isKeyNameValid($keyName)) {
			if (!$this->has($keyName)) {
				$this->_container[$keyName]	= $keyValue;
				$this->$keyName				= $keyValue;
			} else {
				throw new Exception("Add operation. Key '$keyName' already exists!");
			}
		} else {
			throw new Exception("Add operation. Key '$keyName' is not valid!");
		}
	}
	
	/**
	 * Fills the reserved keys names. The user defined keys names must avoid of
	 * using same keys names as the reserved ones.
	 */
	protected function fillReservedKeysNames()
	{
		$this->_reserved_keys = array();
		
		foreach ($this as $key => $value) {
			$this->_reserved_keys[] = (string)$key;
		}
	}
	
	/**
	 * Prepares and normalizes the keys names & the keys values in one general
	 * associative array. The empty keys names are ignored.
	 * 
	 * @param string $keysNames The list of keys names separated by "," symbol.
	 * @param array $keysValues The list of keys values. If this list contains
	 *	only 1 element and this element have an array type - then the rest
	 *	elements are ignored and elements of the 1st element are used like
	 *	arguments to the rest of the keys names.
	 * 
	 * @return array An associative array where the first element represent the
	 *	keys names and the second element represents the keys values.
	 */
	protected function prepareKeysNamesAndValues($keysNames, $keysValues)
	{
		$separator		= ',';
		$keys_names		= explode($separator, $keysNames);
		$keys_values	= $keysValues;
		
		/* Remove all empty keys and spaces in them. */
		$ln = count($keys_names);
		for ($i = 0; $i < $ln; $i++) {
			$normalized_key = trim($keys_names[$i]);
			
			if (!$normalized_key) {
				/* Empty key "" is detected. */
				unset($keys_names[$i]);
			} else {
				$keys_names[$i] = $normalized_key;
			}
		}
		$keys_names = array_values($keys_names);
		
		if (count($keysValues) == 1) {
			if (is_array($keysValues[0])) {
				$keys_values = $keysValues[0];
			}
		}
		
		return array('names' => $keys_names, 'values' => $keys_values);
	}
	
	/**
	 * Merges keys & values by using array shared index.
	 * If count of the key values are bigger than the key names, the rest of
	 * the key values are ignored. Otherwise if count of the key values are
	 * smaller than the key names, the rest of the key values are filling up
	 * with predefined variable DEFAULT_KEY_VALUE.
	 * 
	 * @param array $keysNames
	 * @param array $keysValues
	 */
	protected function fillKeysAndValues($keysNames, $keysValues)
	{
		$i = 0;
		foreach ($keysNames as $key_name) {
			$key_value = isset($keysValues[$i])
				? $keysValues[$i] : self::DEFAULT_KEY_VALUE
			;
			
			$this->add($key_name, $key_value);
			$i++;
		}
	}
	
	/**
	 * Validates the key name.
	 * 
	 * @param string Key name which need to be validated.
	 * 
	 * @return bool TRUE if key name is valid, otherwise FALSE.
	 */
	protected function isKeyNameValid($keyName)
	{
		return array_search($keyName, $this->_reserved_keys, true) === false
			&& preg_match('/^[a-zA-Z_][a-zA-Z0-9_\x7f-\xff]*/', $keyName);
	}
}

}

?>