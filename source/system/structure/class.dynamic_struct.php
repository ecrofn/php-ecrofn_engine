<?php

namespace Ecrofn\System\Structure
{

use Ecrofn\System\Structure\Struct;
use Ecrofn\System\Structure\Exception;

class DynamicStruct extends Struct
{
	/**
	 * Creates a new structure from input data. The structure object represents
	 * itself as object, containing pairs of key & value.
	 * 
	 * The keys values could be assigned by two different ways:
	 * 1. By using the standard method via multiple variable creation.
	 *	<b>$foo = new Structure('bar, baz', $baz, $baz);</b>
	 * 2. By using the array creation method.
	 *	<b>$foo = new Structure('bar, baz', array($baz, $baz));</b>
	 *	An array creation method works when only the input array are passed and
	 *	nothing else. If another variables are passed too, then this array are
	 *	just attaches to the corresponding key and becomes it value.
	 * 
	 * @param string $keysNames [optional] The list of keys names separated by
	 *	',' symbol. If no value are assigned than the empty object will be
	 *	created without any keys names. The empty keys names are ignored.
	 * @param mixed $keysValues [optional] The list of keys values.
	 *	The keys values are aligning by the keys names. If count of the keys
	 *	values are bigger than the keys names, the rest of the keys values are
	 *	ignored. Otherwise if count of the keys values are smaller than the keys
	 *	names, the rest of the missing keys values are filling up with
	 *	predefined variable DEFAULT_KEY_VALUE.
	 * @param mixed $_ [optional]
	 * 
	 * @throws \Ecrofn\System\Structure\Exception
	 */
	public function __construct($keysNames = '', $keysValues = null, $_ = null)
	{
		if ($keysNames == '') {
			$this->fillReservedKeysNames();
			return;
		} else {
			call_user_func_array(array($this, 'parent::__construct'), func_get_args());
		}
	}
	
	/**
	 * Adds a new key to the structure.
	 * 
	 * @param string $keyName The key name to be added.
	 * @param mixed $keyValue The key value.
	 * 
	 * @throws \Ecrofn\System\Structure\Exception
	 */
	public function add($keyName, $keyValue = self::DEFAULT_KEY_VALUE)
	{
		parent::add($keyName, $keyValue);
	}
	
	/**
	 * Removes the selected key from the structure.
	 * 
	 * @param string $keyName The key name to be removed.
	 * 
	 * @throws \Ecrofn\System\Structure\Exception
	 */
	public function remove($keyName)
	{
		if ($this->isKeyNameValid($keyName)) {
			unset($this->_container[$keyName]);
			unset($this->$keyName);
		} else {
			throw new Exception("Remove operation. Key '$keyName' is not valid!");
		}
	}
	
	/**
	 * Cleanups the whole structure by wiping all it keys.
	 */
	public function clear()
	{
		foreach ($this->_container as $key_name => $key_value) {
			$this->remove($key_name);
		}
	}
}

}

?>