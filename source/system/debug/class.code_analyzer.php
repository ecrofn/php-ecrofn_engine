<?php

//http://www.softwareengineeringsolutions.co.uk/class-flags-with-bitwise-operators-2010-05/

namespace Ecrofn\System\Debug
{

class CodeAnalyzer
{
	// Bitwise constant
	const CA_MEMORY	=	1;		// 0000001
	const CA_TIME	=	2;		// 0000010
	const CA_ALL		=	127;	// 1111111
	
	private $start_time;
	private $start_memory;
	
	private $tick_time;
	private $tick_memory;
	
	// List of registered options choosed by user.
	private $options = 0;
	
	public function __construct($options)
	{
		$this->reset();
		$this->selected_options = $options;
	}
	
	public function start()
	{
		$this->reset();
		$this->start_time = $this->microtime_float();
		//$this->start_memory = $this->memory_usage();
	}
	
	public function pause()
	{
		// pause exec.
	}
	
	public function stop()
	{
		// stop exec.
	}
	
	public function getAnalyzeFromLastCall()
	{
		// get analyze between last call and current call.
	}
	
	public function getAnalyze()
	{
		// get analyze from start
		$time	= $this->microtime_float() - $this->start_time;
		//$memory	= $this->memory_usage() - $this->start_memory;
		
		return $time;
	}
	
	private function reset()
	{
		// reset all anaylize
		$this->start_time		= 0;
		$this->start_memory		= 0;
	}
	
	private function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	private function memory_usage()
	{
		//return round((memory_get_usage(true) / 1024), 2);
		return memory_get_usage();
	}
}

}

?>