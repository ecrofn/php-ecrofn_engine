<?php

// Даный класс является абстрактным представлением
// концепции Enum (Перечисление).

abstract class Enum
{
	private static $constCacheArray = NULL;

	private static function getConstants()
	{
		if (self::$constCacheArray == NULL)
		{
			self::$constCacheArray = [];
		}
        
		$calledClass = get_called_class();
		if (!array_key_exists($calledClass, self::$constCacheArray))
		{
			$reflect = new ReflectionClass($calledClass);
			self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function isValidName($name, $strict = false)
	{
		$constants = self::getConstants();

		if ($strict)
		{
			return array_key_exists($name, $constants);
		}

		$keys = array_map('strtolower', array_keys($constants));
		return in_array(strtolower($name), $keys);
	}

	public static function isValidValue($value)
	{
		$values = array_values(self::getConstants());
		return in_array($value, $values, $strict = true);
	}
	
	public static function getValue($name)
	{
		$constants = self::getConstants();
		
		if (array_key_exists($name, $constants))
		{
			return $constants[$name];
		}
		else
		{
			return null;
		}
	}
	
}

// Test example
abstract class Gender extends BasicEnum
{
	const Male = 1;
	const Female = 2;
	const Unknown = 3;
}

echo Gender::isValidName('Male');
echo Gender::isValidValue('2');
echo Gender::getValue('Unknown');

/*echo DaysOfWeek::isValidName('Humpday');                // false
echo DaysOfWeek::isValidName('Monday');                   // true
echo DaysOfWeek::isValidName('monday');                   // true
echo DaysOfWeek::isValidName('monday', $strict = true);   // false
echo DaysOfWeek::isValidName(0);                          // false

echo DaysOfWeek::isValidValue(0);                         // true
echo DaysOfWeek::isValidValue(5);                         // true
echo DaysOfWeek::isValidValue(7);                         // false
echo DaysOfWeek::isValidValue('Friday');                  // false*/

?>
