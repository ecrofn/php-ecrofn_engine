<?php

/*
 * -----------------------------------------------------------------------------
 * Класс Registry - реализация щаблона Registry с небольшими доработками.
 * Использование механизма блокировки предотвращает разработчика от случайного
 * изменения данных в процессе выполнения программы.
 * 
 * @author - Ecrofn.
 * 
 * За основу взят материал с http://amdy.su/pattern-registry/
 * -----------------------------------------------------------------------------
 */

namespace Ecrofn\Storage
{

class Registry
{
	private static $_instance	= null;
	private $data				= array();
	
	private function __construct() {}
	private function __clone() {}
	
	/**
	 * 
	 * @return \Ecrofn\Registry
	 */
	public static function getInstance()
	{
		if(self::$_instance === null) {
			/* \Ecrofn\Registry */
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * Sets new object or modified already existing.
	 * 
	 * @param string $key_name
	 * @param mixed $value
	 * @param bool $locked [optional]
	 * 
	 */
	public function set($key_name, $value, $locked = false)
	{
		if (!$this->isLockExist($key_name)) {
			$this->data[$key_name]['value']		= $value;
			$this->data[$key_name]['locked']	= $locked;
		} else {
			echo 'lock exist';
		}
	}
	
	/**
	 * 
	 * 
	 * @param string $key_name
	 * @return mixed
	 */
	public function get($key_name)
	{
		if ($this->isKeyExist($key_name)) {
			return $this->data[$key_name]['value'];
		}
	}
	
	/**
	 * 
	 * 
	 * @param string $key_name
	 */
	public function lock($key_name)
	{
		if ($this->isKeyExist($key_name)) {
			$this->data[$key_name]['locked'] = true;
		}
	}
	
	/**
	 * 
	 * 
	 * @param string $key_name
	 */
	public function unlock($key_name)
	{
		if ($this->isKeyExist($key_name)) {
			$this->data[$key_name]['locked'] = false;
		}
	}
	
	/**
	 * 
	 * 
	 * @param string $key_name
	 * @return boolean
	 */
	private function isKeyExist($key_name)
	{
		return isset($this->data[$key_name]) ? true : false;
	}
	
	/**
	 * 
	 * 
	 * @param type $key_name
	 * @return boolean
	 */
	private function isLockExist($key_name)
	{
		if (!$this->isKeyExist($key_name)) {
			return false;
		} else {
			return $this->data[$key_name]['locked'];
		}
	}
}

}

?>