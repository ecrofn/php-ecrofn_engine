<?php

namespace Ecrofn\Storage\Session
{

class KeyGroup
{
	private $name;
	private $data;
	private $prefix;
	
	public function __construct($name)
	{
		$this->name = $name;
	}
	
	public function set($name, $value)
	{
		$this->data[$name] = $value;
	}
	
	public function setPrefix($name)
	{
		$this->prefix = $name;
	}
}

}

?>