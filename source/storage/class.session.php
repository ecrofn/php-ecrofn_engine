<?php

namespace Ecrofn\Storage
{

use Exception;

use Ecrofn\Storage\Session\KeyGroup;

class Session
{
	private $started;
	private $prefix;
	private $save_path;
	
	private $groups;
	
	public function __destruct()
	{
		//$this->destroy();
	}
	
	public function start()
	{
		if (!$this->started) {
			
			if (empty($this->save_path)) {
				$this->setSavePath(D_ENGINE_VAR . 'session/');
			}
			
			session_start();
			
			$this->started = true;
		}
	}
	
	public function destroy($remove_data = false)
	{
		if ($this->started) {
			
			if ($remove_data) {
				$_key = $this->calculateKey('');
				
				foreach ($_SESSION as $key => $value) {
					if (stripos($key, $_key) !== false) {
						unset($_SESSION[$key]);
					}
				}
			}
			
			$this->started = false;
			
			session_destroy();
		}
	}
	
	public function get($name)
	{
		$key = $this->calculateKey($name);
		
		if ($this->has($name)) {
			return $_SESSION[$key];
		} else {
			throw new Exception("Key $name is not exist!");
		}
	}
	
	public function set($name, $value)
	{
		$key = $this->calculateKey($name);
		
		$_SESSION[$key] = $value;
	}
	
	public function has($name)
	{
		$key = $this->calculateKey($name);
		
		return isset($_SESSION[$key]);
	}
	
	public function remove($name)
	{
		$key = $this->calculateKey($name);
		
		if ($this->has($name)) {
			unset($_SESSION[$key]);
		} else {
			throw new Exception("Key $name is not exist!");
		}
	}
	
	public function setPrefix($name)
	{
		$this->prefix = $name;
	}
	
	public function setSavePath($path)
	{
		$this->save_path = $path;
		session_save_path($path);
	}
	
	public function attachGroup($name, KeyGroup $group)
	{
		$this->groups[$name] = $group;
	}
	
	public function removeGroup($name)
	{
		unset($this->groups[$name]);
	}
	
	public function isStarted()
	{
		return $this->started;
	}
	
	public function getPrefix()
	{
		return $this->prefix;
	}
	
	private function calculateKey($name)
	{
		return !empty($this->prefix) ? $this->prefix . '#' . $name : $name;
	}
}

}

?>