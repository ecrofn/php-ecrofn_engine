<?php

namespace Ecrofn\GUI
{

class General
{
	/**
	 * Draws navigation line.
	 * 
	 * @param array $data Indexed array. Each key represent string value.
	 * @param string $separator
	 * 
	 * @return string HTML representated navigation line.
	 */
	public static function drawNavigationLine(array $data, $separator = ' > ')
	{
		$result = '';
		$length = count($data);
		
		/* Empty array checking. */
		if ($length != 0)
		{
			if ($length == 1)
			{
				$result = $data[0];
			}
			else
			{
				for ($counter = 0; $counter < $length - 1; $counter++)
				{
					$result .= (string)$data[$counter] . $separator;
				}
				$result .= $data[$length - 1];
			}
		}
		
		return $result;
	}
	
	public static function convertAssocToIndex(array $data)
	{
		/**
		 * Also as analog we can use built-in RecursiveArrayIterator &
		 * RecursiveIteratorIterator objects, but tests on 1000000 loop
		 * iterations (10 elements array) shows that it's slower more than 2x
		 * times(56 sec. vs 121 sec.) but have bit less memory usage
		 * (407 kb vs 412 kb).
		 * 
		 * http://habrahabr.ru/post/31422/
		 */
		
		static $result	= array();
		static $depth	= 0;
		
		/**
		 * As this variables are static, we need to reset it before next
		 * method call, otherwise result from previous method call will be
		 * exist when calculation is finished.
		 */
		if ($depth == 0)
		{
			$result = [];
		}
		
		foreach ($data as $key)
		{
			if (\is_array($key))
			{
				$depth++;
				self::convertAssocToIndex($key);
				$depth--;
			}
			else
			{
				$result[] = $key;
			}
		}
		
		return $result;
	}
}

}

?>