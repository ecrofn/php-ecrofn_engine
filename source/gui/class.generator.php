<?php

namespace Ecrofn\Gui
{

use Ecrofn\System\FileSystem\File;

class Generator
{
	/** @var array Set of RegEx rules. Used while template generation. */
	private $_rules = array();	
	/** @var array Set of predefinied delimeters for RegEx rules. Used while template generation. */
	private $_delimeters = array(
		'LEFT_SIDE'		=> '{',
		'RIGHT_SIDE'	=> '}'
	);
	
	private $DOCUMENT_ENCODING	= 'UTF-8';
	private $MATCHING_ERROR		= '[UNSUPPORTED_COMMAND %NAME%]';
	
	// Если истина, то все конструкции при генерации дополняются оберткой
	// empty, даже если они не используют безопасный вариант - var@ / var
	private $safeMode = false;
	
	public function __construct($sm = false)
	{
		$this->safeMode = $sm;
	}
	
	public function loadConvertRules($path)
	{
		$this->_rules = require_once $path;
	}
	
	// template_source - путь к  файлу исходнику.
	// template_compiled - путь к файлу после компиляции.
	public function generate($template_source, $template_compiled)
	{
		if (!File::exists($template_source)) {
			echo('File ' . $template_source . ' not found!');
			return;
		}
		
		if ($template_source == $template_compiled) {
			echo('Different names/path required!');
			return;
		}
		
		$f = new File($template_source);
		
		$text		= str_replace(array('<?', '?>'), array('&lt;?', '?&gt;'), $f->getContent());
		unset($f);
		$pattern	= '#(\\' . $this->_delimeters['LEFT_SIDE'] . '.*\\' . $this->_delimeters['RIGHT_SIDE'] . ')#sU';
		/* Исходный шаблон разделяем по частям, нам интересны только те части,
		 * которые соответствуют указанной маске поиска = $_delimeters.
		 * Части не соответствующие маске поиска тоже попадут в результирующий
		 * массив. Они понадобятся позже, когда будет производиться сборка
		 * откомпилированного шаблона.
		*/
		$parts			= preg_split($pattern, $text, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
		$compiled_text	= '';
		
		foreach ($parts as $part) {
			
			$isPhpCode = false;
			
			/* Проверка очередной части на соответствие маске поиска $_delimeters. */
			if ($part[0] == $this->_delimeters['LEFT_SIDE'] &&
				$part[mb_strlen($part, $this->DOCUMENT_ENCODING) - 1] == $this->_delimeters['RIGHT_SIDE']
			) {
				/* Удаление служебных символов-разделителей для получения чистого макросного выражения. */
				$macro = substr($part, 1, -1);
				
				/* Попытка найти шаблон замены макросного выражения. */
				$part = $this->returnMatch($macro);
				
				if ($part != $this->MATCHING_ERROR) {
					$isPhpCode = true;
				} else {
					$part = str_replace('%NAME%', $macro, $part);
				}
			}
			
			if ($isPhpCode) {
				$compiled_text .= '<?php ' . $part . ' ?>';
			} else {
				$compiled_text .= $part;
			}
		}
		
		$f = new File($template_compiled);
		$f->setContent($compiled_text);
		unset($f);
		//file_put_contents($template_compiled, $compiled_text);
		
		//file_put_contents($template_compiled, $this->deleteEmptyLines($compiled_text));
	}
	
	/*
	 * Метод ищет соответствие в словаре правил для передаваемого макроса.
	 * При удачном поиске возвращает подготовленное выражение, в случае неудачи
	 * предопределенное сообщение с ошибкой.
	 */
	private function returnMatch($macro)
	{
		$part = $this->MATCHING_ERROR;
		
		foreach ($this->_rules as $pattern => $replace)
		{
			if (preg_match($pattern, $macro))
			{
				$part = preg_replace($pattern, $replace, $macro);
				break;
			}
		}
		
		return $part;
	}
	
	/*
	 * Метод удаляет все пустые строки из скомпилированного шаблона, для
	 * устранения эффекта разрыва при просмотре html-кода страницы.
	 */
	private function deleteEmptyLines($source)
	{
		return preg_replace("/[\r\n]+/s", "\n",
							preg_replace("/[\r\n][ \t]+/s", "\n", $source));
	}
}

}