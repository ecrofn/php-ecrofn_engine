<?php

namespace Ecrofn\GUI
{

class Renderer
{
	private $rendered_objects;
	
	private $renderer_list	= null;
	private $renderer_data	= array();
	
	public function __construct()
	{
		$this->rendered_objects = 0;
	}
	
	/**
	 * Outputs file contents to the output stream.
	 * 
	 * @param string $path Path to file.
	 */
	public function render($path)
	{		
		if (\file_exists($path))
		{
			include $path;
			$this->rendered_objects++;
		}
		else
		{
			echo '<br />TEMPLATE \'' . $path . '\' NOT EXIST, REPLACE IT CORRECTLY';
		}
	}
}

}