<?php

/**
 * This file contains special set of rules, which are uses during compilation
 * by \Ecrofn\GUI\Generator class. 
 * 
 * Additional info about dolar signs & how to handle it:
 * https://stackoverflow.com/questions/4534516/regex-1-2-etc
 */

return array(
	/* Print variable value. */
	'#^pvar\((.+)\)$#'						=> '$this->echoString($this->renderer_data->variable_data[\'\1\']);',
	
	//'#^pvar\((.+\((.+\)))\)$#'
	
	/* If variable exist, print it value. */
	'#^pvar@\((.+)\)$#'						=> 'if (!empty($this->renderer_data->variable_data[\'\1\'])) { $this->echoString($this->renderer_data->variable_data[\'\1\']); } else { echo(\'EMPTY VARIABLE VALUE\'); }',
	/* Print variable name. This is helpful in expressions. */
	'#^var\((.+)\)$#'						=> '$this->renderer_data->variable_data[\'\1\'];',
	
	/* Print local variable value. */
	'#^plvar\((.+)\)$#'						=> '$this->echoString($\1);',
	/* If local variable exist, print it value. */
	'#^plvar@\((.+)\)$#'					=> 'if (!empty($\1)) { $this->echoString($\1); } else { echo(\'EMPTY LOCAL VARIABLE VALUE\'); }',
	/* Use local variable name. This is helpful in expressions. */
	'#^lvar\((.+)\)$#'						=> '$\1;',
	
	/* Print constant variable value. */
	'#^pconst\((.+)\)$#'					=> '$this->echoString(\1);',
	/* Print constant variable name. This is helpful in expressions. */
	'#^const\((.+)\)$#'						=> '\1;',
	
	/* Launches foreach loop, which iterate over selected variable value by using local variable. */
	'#^foreach\s+([^,]+)\s+as\s+(.*)$#'		=> 'foreach ($this->renderer_data->variable_data[\'\1\'] as $\2):',
	/* Launches foreach loop, which iterate over selected local variable value by using another local variable. */
	'#^lforeach\s+([^,]+)\s+as\s+(.*)$#'	=> 'foreach ($1 as $2):',
	/* Ends open foreach/lforeach loop. */
	'#^/foreach$#'							=> 'endforeach;',
	
	/* Launches rendering process, as standalone method. */
	'#^render\((.+)\)$#'					=> '$this->render(\1);',
	
	// Comment block.
	'#^comnt\((.+)\)$#'						=> '/* (\1) */',
	
	'#^comnt\((.+)\)$#'						=> '/* (\1) */',
	
	'#^block\((.+)\)$#'						=> '$this->blockBegin(\'\1\');',
	'#^/block\((.+)\)$#'					=> '$this->blockEnd(\'\1\');',
	
	
	//'#^direct_input\s\(\s+(.*)\s\)$#'		=> '$1'
	/* Direct input doesn't change anything, it's only puts all data in brackets. New lines is allowed. */
	'#^direct\((.*)\)$#s'			=> '$1'
);

?>