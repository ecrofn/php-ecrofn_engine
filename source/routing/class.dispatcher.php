<?php

namespace Ecrofn\Routing
{

class Dispatcher
{
	public function __construct(\Ecrofn\Routing\Route $route)
	{
		$this->dispatch($route);
	}
	
	public function dispatch(\Ecrofn\Routing\Route $route)
	{
		if (file_exists(F_MODULE_INDEX)) {
			include F_MODULE_INDEX;
		}
		
		$controller = \Ecrofn\Mvc\MvcFactory::create($route->getClass(), \Ecrofn\Mvc\MvcFactory::PARENT_CONTROLLER_CLASS);
		$controller->invokeMethod($route->getAction(), $route->getParameters());
	}
}

}

?>