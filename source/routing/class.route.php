<?php

// 15/10/2015.

namespace Ecrofn\Routing
{

use Ecrofn\System\Structure\Struct;

class Route
{
	private $pattern;
	private $class;
	private $action;
	private $parameters;
	private $http_methods;
	
	private $_compiled = false;
	
	public function __construct($pattern = '', $class = '', $action = '', $parameters = '', $method = '')
	{
		$this->setPattern($pattern);
		$this->setClass($class);
		$this->setAction($action);
		$this->setParameters($parameters);
		$this->setMethod($method);
	}
	
	public function getPattern()
	{
		return $this->pattern;
	}
	
	public function getClass()
	{
		return $this->class;
	}
	
	public function getAction()
	{
		return $this->action;
	}
	
	public function getParameters()
	{
		if (!$this->_compiled) {
			$this->compileParameters(array());
		}
		return $this->parameters;
	}
	
	public function getMethods()
	{
		return $this->http_methods;
	}
	
	public function hasStrictMode()
	{
		return $this->strict_mode;
	}
	
	public function setPattern($pattern)
	{
		$this->pattern = $pattern;
	}
	
	public function setClass($class)
	{
		$this->class = $class;
	}
	
	public function setAction($method)
	{
		$this->action = $method;
	}
	
	/**
	 *
	 * @param string $parameters List of comma separated names.
	 */
	public function setParameters($parameters)
	{
		$this->parameters = $parameters;
	}
	
	/**
	 * Sets HTTP type of route.
	 *
	 * $route->setMethod('GET');
	 * $route->setMethod('POST');
	 *
	 * @param string $method HTTP type of route.
	 */
	public function setMethod($method)
	{
		$this->http_methods = $method;
	}
	
	/**
	 * Transforms string representated route parameters to
	 * \Ecrofn\System\Structure\Struct object.
	 *
	 * @param array $data Array, where each index represent some value,
	 * which will be assigned to corresponding key.
	 */
	public function compileParameters(array $data)
	{
		$values = array();
		
		$length = count($data) - 1;
		$counter = 0;
		
		$keys = '';
		
		foreach ($data as $key => $value) {
			if ($counter < $length) {
				$keys .= "$key,";
			} else {
				$keys .= $key;
			}
			$values[] = $value;
		}
		
		if (!empty($this->parameters)) {
			$keys = $keys . ',' . $this->parameters;
		}
		
		$this->parameters = new Struct($keys, $values);
		
		$this->_compiled = true;
		
		//var_dump($this->parameters);
	}
}

}

?>