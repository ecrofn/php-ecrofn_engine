<?php

// http://jakulov.ru/blog/2014/bun_framework_http

namespace Ecrofn\Routing
{

class Request
{
	/**
	 * @param null $param
	 *
	 * @return array|mixed|null
	 */
	public function getPost($param = null)
	{
		return $this->getArrayFieldParam('_POST', $param);
	}
	
	public function getQuery($param = null)
	{
		return $this->getArrayFieldParam('_GET', $param);
	}
	
	/**
	 * Alias of getQuery method.
	 * 
	 * @param type $param
	 * @return type
	 */
	public function getGet($param = null)
	{
		return $this->getQuery($param);
	}
	
	public function getQueryString()
	{
		$query_string = $this->getArrayFieldParam('_SERVER', 'QUERY_STRING');
		
		return (isset($query_string) ? $query_string : '');
	}
	
	public function getUri()
	{
		$request_uri = $this->getArrayFieldParam('_SERVER', 'REQUEST_URI');
		
		return (isset($request_uri) ? $request_uri : '');
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getMethod()
	{
		$method = $this->getArrayFieldParam('_SERVER', 'REQUEST_METHOD');
		
		return (isset($method) ? strtoupper($method) : '');
	}
	
	/**
	 * @return null|string
	 */
	public function getIp()
	{
		$ip = $this->getArrayFieldParam('_SERVER', 'REMOTE_ADDR');
		
		return (isset($ip) ? $ip : '127.0.0.1');
	}
	
	/**
	 * @return string
	 */
	public function getUserAgent()
	{
		$method = $this->getArrayFieldParam('_SERVER', 'USER_AGENT');
		
		return (isset($method) ? $method : '');
	}
	
	public function isAjax()
	{
		$ajax = $this->getArrayFieldParam('_SERVER', 'X_HTTP_REQUEST_WITH');
		
		return (isset($ajax) && strtolower($ajax) == 'xhrhttprequest');
	}
	
	public function isPost()
	{
		return $this->getMethod() == 'POST';
	}
	
	public function isGet()
	{
		return $this->getMethod() == 'GET';
	}
	
	protected function getArrayFieldParam($array_field, $param = null)
	{
		$root_element = $GLOBALS[$array_field];
		
		if ($param == null) {
			return $root_element;
		}
		elseif (isset($root_element[$param])) {
			return $root_element[$param];
		}
		
		return null;
	}
}

}

?>