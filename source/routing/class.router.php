<?php

/** @todo 06.06.2016 Hard coded dependecy - Request class! */

/** @todo 22.06.2016 Add native types for route parameters. (string). */

namespace Ecrofn\Routing
{

use Ecrofn\Routing\Route;
use Ecrofn\Routing\Request;

class Router
{
	private $routes			= array();
	private $error_route	= null;
	private $matched_route	= null;
	
	private $route_prefix	= '';
	
	private $http_methods	= array(
		'GET',
		'POST',
		'PUT',
		'DELETE',
		'HEAD'
	);
	private $http_patterns	= array(
		'int'	=> '[0-9]+',
		'flt'	=> '[0-9]+',
		'str'	=> '[a-zA-Z\.\-_%]+',
		'any'	=> '[a-zA-Z0-9\.\-_%]+'
	);
	
	private $remove_trailing_slashes	= true;
	private $request					= null;
	
	public function __construct()
	{
		$this->request = new Request();
		
		$this->prepareRoutes();
		$this->loadRoutes();
	}
	
	public function add($pattern, $handler, $parameters = '', $methods = '')
	{
		$route = $this->create($pattern, $handler, $parameters, $methods);
		$_methods = $route->getMethods();
		
		/* Attach route object to corresponding http methods. */
		foreach ($_methods as $method) {
			$this->routes[$method][$pattern] = $route;
		}
	}
	
	public function addGet($pattern, $handler, $parameters = '')
	{
		$this->add($pattern, $handler, $parameters, 'GET');
	}
	
	public function addPost($pattern, $handler, $parameters = '')
	{
		$this->add($pattern, $handler, $parameters, 'POST');
	}
	
	public function setError($handler, $parameters = '')
	{
		$route = $this->create('', $handler, $parameters, $this->request->getMethod());
		$this->error_route = $route;
	}
	
	public function setRoutePrefix($prefix = '/')
	{
		$this->route_prefix = $prefix;
	}
	
	public function getRoutes()
	{
		return $this->routes;
	}
	
	public function getMatchedRoute()
	{
		return $this->matched_route;
	}
	
	public function removeTrailingSlashes($flag)
	{
		$this->remove_trailing_slashes = $flag;
	}
	
	public function handle($uri = '')
	{
		$this->doMatch($uri);
		
		new \Ecrofn\Routing\Dispatcher($this->matched_route);
	}
	
	private function create($pattern, $handler, $parameters, $methods)
	{
		$pcre_begin	= '#';
		$pcre_end	= '$#';
		
		/* Generate raw pattern from macros pattern. */
		$_pattern = $pcre_begin . preg_replace_callback(
			
			/*
			 * Constructions, which have pattern like (int:id) will be
			 * transformed to the raw code, where value of
			 * 
			 * int	= type of parameter ($this->http_patterns allowed values).
			 * :		= separator value.
			 * id	= name of parameter.
			 */
			
			'#\((\w+):(\w+)\)#',
			function($matches) use ($pattern) {
				/* First element [0] contains the whole string, we dont need it. */
				$pattern	= $matches[1];
				$name		= $matches[2];
				
				/*
				 * Finalize job with generating named subpatterns & replacing
				 * parameter aliases with raw values.
				 */
				return '(?<' . $name . '>' . strtr($pattern, $this->http_patterns) . ')';
			},
			$pattern
		) . $pcre_end;
		
		$_tmp		= explode(':', $handler);
		$_class		= $_tmp[0];
		$_action	= isset($_tmp[1]) ? $_tmp[1] : '';
		
		$_methods = $this->configureHttpMethods($methods);
		
		$route = new Route(
			$_pattern,
			$_class,
			$_action,
			$parameters,
			$_methods
		);
		
		return $route;
	}
	
	private function doMatch($uri)
	{		
		/* If URI is empty, information from rewrite engine will be taken. */
		$url_path		= empty($uri) ?
							$this->request->getUri() :
							$uri;	
		$http_method	= $this->request->getMethod();
		
		$was_matched	= false;
		$parameters		= array();
		
		foreach ($this->routes[$http_method] as $route) {
			
			$url_pattern = $route->getPattern();
			
			if (preg_match($url_pattern, $url_path, $parameters)) {
				$this->matched_route	= $route;
				$was_matched			= true;
				
				if ($this->request->isGet()) {
					
					/* Strict mode. */
					foreach ($parameters as $key => $value) {
						if (is_int($key)) {
							unset($parameters[$key]);
						}
					}
					
					/* Native mode not finished yet. */
					
				} elseif ($this->request->isPost()) {
					$parameters = $this->request->getPost();
				}
				
				break;
			}
		}
		
		if (!$was_matched) {
			$this->matched_route = $this->error_route;
		}
		
		$this->matched_route->compileParameters($parameters);
	}
	
	private function prepareRoutes()
	{
		foreach ($this->http_methods as $method) {
			$this->routes[$method] = array();
		}
	}
	
	private function configureHttpMethods($methods)
	{
		if (!is_array($methods)) {
			$methods = array($methods);
		}
		
		foreach ($methods as &$method) {
			$method = strtoupper($method);
			
			if (array_search($method, $this->http_methods) === false) {
				$method = 'GET';
			}
		}
		
		/**
		 * Remove duplicate keys.
		 * http://php.net/manual/ru/function.array-unique.php#77743
		 */
		return array_keys(array_flip($methods));
	}
	
	private function loadRoutes()
	{
		/* Load engine defined routes. */
		require_once D_ENGINE_PRIVATE . 'inc.routes.php';
		
		/* Load user defined routes.
		 * If some of required routes are missing (404 error etc.),
		 * engine routes will be used, otherwise required routes will be
		 * overwrited by user routes.
		 */
		if (\file_exists(D_MODULE_ROOT . 'var/inc.routes.php'))
		{
			require_once D_MODULE_ROOT . 'var/inc.routes.php';
		}
	}
}

}

?>