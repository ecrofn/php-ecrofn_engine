<?php

// Роутеры:
// https://toster.ru/q/178365
// http://www.itlessons.info/php/routing-library/

namespace Ecrofn\Routing
{

/*
 * Класс Router.
 */
class Router
{
	/** @var \Ecrofn\Routing\RoutesContainer List of available routes. */
	private $routes				= null;
	private $route_prefix		= null;
	
	/** @var \Ecrofn\Routing\Request Wrapper via _HTTP & _SERVER globals. */
	private $request			= null;
	/** @var \Ecrofn\Routing\Route */
	private $route				= null;
	
	public function __construct()
	{
		$this->loadRoutesDictionary();
	}
	
	public function run()
	{
		$this->matchObject();
		
		$dispatcher = new \Ecrofn\Routing\Dispatcher($this->getMatchedRoute());
	}
	
	public function getMatchedRoute()
	{
		return $this->route;
	}
	
	public function setMatchedRoute(\Ecrofn\Routing\Route $route)
	{
		$this->route = $route;
	}
	
	public function setRequest(\Ecrofn\Routing\Request $request)
	{
		$this->request = $request;
	}
	
	public function setRoutePrefix($prefix = '/')
	{
		$this->route_prefix = $prefix;
	}
	
	/** 
	* Loads prepared routes dictionary.
	*/
	private function loadRoutesDictionary()
	{
		$this->routes = new \Ecrofn\Routing\RoutesContainer();
		
		/* Load engine defined routes. */
		require_once D_ENGINE_PRIVATE . 'inc.routes.php';
		
		/* Load user defined routes.
		 * If some of required routes are missing (404 error etc.),
		 * engine routes will be used, otherwise required routes will be
		 * overwrited by user routes.
		 */
		if (\file_exists(D_MODULE_ROOT . 'var/inc.routes.php'))
		{
			require_once D_MODULE_ROOT . 'var/inc.routes.php';
		}
	}
	
	private function matchObject()
	{
		$pcre_begin		= '#';
		$pcre_end		= '$#';
		
		$url_path		= $this->request->getUri();
		
		$matched		= false;
		$matches		= array();
		
		$routes = $this->routes->getRoutes();
		
		foreach ($routes as $route)
		{
			$url_pattern = $pcre_begin . $this->route_prefix . $route->getPattern() . $pcre_end;
			
			if (preg_match($url_pattern, $url_path, $matches))
			{
				$this->route = $route;
				
				/* Remove first parameter - whole URI name. */
				array_shift($matches);
				
				$matched	= true;
				
				break;
			}
		}
		
		if (!$matched)
		{
			/* Set default dispatcher struct if all other matches failed. */
			$this->route = $this->routes->getErrorRoute();
		}
		
		$this->route->compileParameters($matches);
	}
}

}
?>
