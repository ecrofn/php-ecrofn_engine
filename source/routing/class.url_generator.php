<?php

// 01/06/2015.

// Этот класс предназначен для генерации УРЛОВ.
// Добавить механизм проверки корректности УРЛА при его инициализации. За
// основу взять словарь путей.

namespace Ecrofn\Routing
{

class UrlGenerator
{
	private $trailing_symbols;
	
	public function __construct()
	{
		$this->trailing_symbols = '';
	}
	
	public function setTrailingSymbols($data = '/')
	{
		$this->trailing_symbols = $data;
	}
	
	public function generate($data)
	{
//		$len = strlen($data);
//		
//		/* Cut last symbol, ($), will be chaned soon after testing. */
//		return substr($data, $len - 1) . $this->trailing_symbols;		
		return $data;
	}
}

}

?>
