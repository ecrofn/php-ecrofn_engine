<?php

namespace Ecrofn\Event
{

use Ecrofn\Event\EventInterface;
use Ecrofn\Event\EventableInterface;

interface EventManagerInterface
{
	public function attachEvent(EventInterface $event, $name, $active);
	
	public function detachEvent($name);
	
	public function fireEvent($name, EventableInterface $context);
}

}

?>