<?php

namespace Ecrofn\Event
{

interface EventInterface
{
	public function setAction($action);
	
	public function getAction();
}

}

?>