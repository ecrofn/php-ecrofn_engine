<?php

namespace Ecrofn\Event
{

use Ecrofn\Event\EventInterface;
use Ecrofn\Event\EventableInterface;

use ReflectionFunction;

class Event implements EventInterface
{
	private $action;		// Имя метода, на которое будет устанавливаться событие
	
	private $method;		// Замыкание или анонимка, которое будет замещать/дополнять существующий метод объекта
	private $params;		// Параметры метода без учета OBJ, массив
	
	private $in_object_context	= false;
	private $obj_param_index	= -1;	// Порядок obj для корректного вызова метода.
	
	private $active = false;
	
	public function __construct($action = '', $method = null, $params = array())
	{
		$this->setAction($action);
		if ($method) {
			$this->setMethod($method, $params);
		}
	}
	
	/**
	 * 
	 * @return void
	 */
	public function suspend()
	{
		$this->active = false;
	}
	
	/**
	 * 
	 * @return void
	 */
	public function resume()
	{
		$this->active = true;
	}
	
	/**
	 * 
	 * @return bool
	 */
	public function isActive()
	{
		return $this->active;
	}
	
	/**
	 * 
	 * @return bool
	 */
	public function inObjectContext()
	{
		return $this->in_object_context;
	}
	
	/**
	 * 
	 * @param string $action
	 */
	public function setAction($action)
	{
		$this->action = $action;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}
	
	/**
	 * 
	 * @param callable $method
	 * @param array $params [optional]
	 */
	public function setMethod(callable $method, $params = array())
	{
		$this->method = $method;
		$this->setParams($params);
	}
	
	/**
	 * 
	 * @return callable
	 */
	public function getMethod()
	{
		return $this->method;
	}
	
	/**
	 * Invokes current event.
	 * 
	 * @param EventableInterface $context [optional]
	 * The object, which corresponds to the required interface. Should be used
	 * only when object context is allowed in current event, otherwise ignored.
	 */
	public function fire(EventableInterface $context = null)
	{
		// http://stackoverflow.com/questions/4535330/calling-closure-assigned-to-object-property-directly
		
		//$this->method->__invoke();
		
		if ($this->active) {
			$p = $this->compileParams($context);
			call_user_func_array($this->method, $p);
		}
	}
	
	/**
	 * Prepares event parameters without object context.
	 * If count of event parameters values is less than count of event
	 * parameters keys then missing values automatically filled up with
	 * default values, otherwise if count is bigger, the rest of values
	 * are ignored.
	 * 
	 * @param array $params Event parameters values. Array should be indexed.
	 */
	private function setParams(array $params)
	{
		$this->params = [];
		
		// Get events parameters names.
		$r = new ReflectionFunction($this->method);
		
		$names			= $r->getParameters();
		$values			= $params;
		$values_count	= count($values);
		
		$i = 0;
		foreach ($names as $n) {
			$name = $n->getName();
			
			// Object context.
			if ($name == 'obj') {
				$this->in_object_context = true;
				$this->obj_param_index = $i;
				continue;
			}
			
			if ($i < $values_count) {
				// Parameter value.
				$this->params[$name] = $values[$i];
			} else {
				// Default value.
				$this->params[$name] = null;
			}
			
			$i++;
		}
	}
	
	/**
	 * Compiles event parameters with object context.
	 * 
	 * @param EventableInterface $context [optional]
	 * The object, which corresponds to the required interface. Should be used
	 * only when object context is allowed in current event, otherwise ignored.
	 * 
	 * @return array Associative array, where each key represents parameter
	 * name and each value represents parameter value.
	 */
	private function compileParams(EventableInterface $context = null)
	{
		$p = array();
		
		foreach ($this->params as $value) {
			$p[] = $value;
		}
		
		// Insert context object in the required order.
		if ($this->in_object_context && $context) {
			array_splice($p, $this->obj_param_index, 0, array($context));
		}
		
		return $p;
	}
}

}

?>