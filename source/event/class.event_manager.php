<?php

namespace Ecrofn\Event
{

use Ecrofn\Event\EventManagerInterface;
use Ecrofn\Event\EventableInterface;
use Ecrofn\Event\Exception;

class EventManager implements EventManagerInterface
{
	private $events = array();
	
	public function __construct($events = array())
	{
		foreach ($events as $event) {
			$this->attachEvent($event);
		}
	}
	
	/**
	 * 
	 * @param \Ecrofn\Event\EventInterface $event
	 * @param string $name
	 * @param bool $active
	 */
	public function attachEvent(EventInterface $event, $name = '', $active = true)
	{
		if (!$name) {
			$name = $event->getAction();
		}
		
		if ($active) {
			$event->resume();
		} else {
			$event->suspend();
		}
		$this->events[$name] = $event;
	}
	
	/**
	 * 
	 * @param type $name
	 */
	public function detachEvent($name)
	{
		if ($this->eventExists($name)) {
			unset($this->events[$name]);
		}
	}
	
	/**
	 * 
	 * @param string $name
	 */
	public function suspendEvent($name)
	{
		$event = $this->getEvent($name);
		
		$event->suspend();
	}
	
	/**
	 * 
	 * @param string $name
	 */
	public function resumeEvent($name)
	{
		$event = $this->getEvent($name);
		
		$event->resume();
	}
	
	/**
	 * 
	 * @param type $name
	 * @param EventableInterface $context
	 * 
	 * @return boolean
	 * 
	 * @throws Exception
	 */
	public function fireEvent($name, EventableInterface $context = null)
	{
		if ($this->eventExists($name)) {
			
			$event = $this->events[$name];
			
			if ($event->inObjectContext() && !$context) {
				throw new Exception("Object context required!");
			}
			$event->fire($context);
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param string $name
	 * 
	 * @return type
	 * 
	 * @throws Exception
	 */
	public function getEvent($name)
	{
		if ($this->eventExists($name)) {
			return $this->events[$name];
		} else {
			throw new Exception("Event $name not registered in current event manager!");
		}
	}
	
	/**
	 * 
	 * @param type $name
	 * 
	 * @return bool
	 */
	public function eventExists($name)
	{
		return isset($this->events[$name]);
	}

}

}

?>