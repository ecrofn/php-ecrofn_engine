<?php

namespace Ecrofn\Event
{

use Ecrofn\Event\EventManagerInterface;

interface EventableInterface
{
	public function setEventsManager(EventManagerInterface $event_manager);
	
	public function getEventsManager();
}

}

?>