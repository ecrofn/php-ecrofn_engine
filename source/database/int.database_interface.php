<?php

namespace Ecrofn\Database
{

interface DatabaseInterface
{
	public function connect();
	
	public function isConnected();
	
	public function disconnect();
	
	public function setQueryText($text);
	
	public function hasQueryParam($name);
	
	public function setQueryParam($name, $value);
	
	public function getQueryParam($name = '');
	
	public function executeQuery($query_text = '');
}

}

?>