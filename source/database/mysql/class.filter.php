<?php

namespace Ecrofn\Database\Mysql
{

class Filter
{
	public static function sanitize($value)
	{
		$type = gettype($value);
		$prepared = null;
		
		switch ($type)
		{
			case 'string'	:
				$prepared = self::sanitizeToString($value);
				break;
			case 'integer'	:
				$prepared = self::sanitizeToInt($value);
				break;
			case 'boolean'	:
				$prepared = self::sanitizeToBool($value);
		}
		
		return $prepared;
	}
	
	private static function sanitizeToBool($value)
	{
		$result = $value ? 'TRUE' : 'FALSE';
		
		return $result;
	}
	
	private static function sanitizeToInt($value)
	{
		$result = (string) $value;
		
		return $result;
	}
	
	private static function sanitizeToFloat($value)
	{
		$result = (string) $value;
		
		return $result;
	}
	
	private static function sanitizeToString($value)
	{
		$result = '\'' . str_replace("'", "\'", $value) . '\'';
		
		return $result;
	}
	
	private static function sanitizeToArray($value)
	{
		
	}
	
	private static function sanitizeToObject($value)
	{
		$result = $value->__toString();
		
		return $result;
	}
	
	private static function sanitizeToResource()
	{
		
	}
	
	private static function sanitizeToNULL()
	{
		$result = 'NULL';
		
		return $result;
	}
	
	private static function sanitizeToUnknown()
	{
		
	}
}

}

?>