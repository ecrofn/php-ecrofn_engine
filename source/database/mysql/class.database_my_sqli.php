<?php

/** @todo 14.07.2016 filterQueryParam method works incorrectly with LIKE operand.
 * It will add sanitize symbols while LIKE automatically = string type, so no
 * symbols are required.
 */

namespace Ecrofn\Database\Mysql
{

use mysqli;
use mysqli_result;

use Exception;

use Ecrofn\Database\AbstractDatabase;
use Ecrofn\Database\Mysql\DatabaseMySqliResult;

use Ecrofn\Database\Mysql\Filter;

class DatabaseMySqli extends AbstractDatabase
{
	private $mysqli_instance	= null;
	
	/**
	 * Non blank string made intentional, because my_sqli throws exception
	 * when blank query is executing.
	 */
	private $query_text			= ' ';
	private $query_params		= array();
	
	private $connected			= false;
	
	private $host;
	private $username;
	private $password;
	private $dbname;
	private $port;
	private $charset;
	private $table_prefix;
	
	public function __construct($host = null, $username = null,
		$password = null, $dbname = null, $port = null,
		$charset = 'utf8', $tp = ''
	)
	{
		$this->host			= $host;
		$this->username		= $username;
		$this->password		= $password;
		$this->dbname		= $dbname;
		$this->port			= $port;
		$this->charset		= $charset;
		$this->table_prefix	= $tp;
	}
	
	public function __destruct()
	{
		$this->disconnect();
	}
	
	public function connect()
	{
		if ($this->mysqli_instance == null)
		{
			try
			{
				$this->mysqli_instance = new mysqli($this->host, $this->username, $this->password, $this->dbname, $this->port);
				$this->mysqli_instance->set_charset($this->charset);
				
				$this->connected = true;
			}
			catch(Exception $e)
			{
				throw new Exception(DB_ERROR_INIT);
			}
		}
		else
		{
			echo DB_NOTIFY_ALREADY_CONNECTED;
		}
	}
	
	public function disconnect()
	{
		if ($this->mysqli_instance != null)
		{
			$this->mysqli_instance->close();
			$this->mysqli_instance = null;
			
			$this->connected = false;
		}
	}
	
	public function setQueryText($text)
	{
		$this->query_text = trim($text);
	}
	
	public function getQueryText()
	{
		return $this->query_text;
	}
	
	public function getCompiledQueryText()
	{
		return $this->prepareQueryText();
	}
	
	public function hasQueryParam($name)
	{
		return (isset($this->query_params[$name]));
	}
	
	public function setQueryParam($name, $value)
	{
		$value_type = gettype($value);
		if ($value_type == 'integer' || $value_type == 'string' || $value_type = 'boolean')
		{
			$this->query_params[$name] = $value;
		}
		else
		{
			throw new Exception('Only int, string or bool type values are allowed!');
		}
	}
	
	public function getQueryParam($name = '')
	{
		$result = null;
		
		if (empty($name)) {
			$result = $this->query_params;
		} else {
			if ($this->hasQueryParam($name)) {
				$result = $this->query_params[$name];
			} else {
				throw new Exception("Parameter $name is not exist!");
			}
		}
		
		return $result;
	}
	
	public function resetQueryParams()
	{
		$this->query_params = [];
	}
	
	/**
	 * Executes query.
	 * 
	 * @param string $query_text Query text.
	 * @return \Ecrofn\DataBase\DataBaseMySQLIResult Result object.
	 * @throws \Exception
	*/
	public function executeQuery($query_text = '')
	{
		if (!$this->connected) {
			throw new \Exception('Database not connected!');
		}
		
		$result = false;
		
		$_query_text = '';
		
		if (!empty($query_text)) {
			$_query_text = $query_text;
		} else {
			$_query_text = $this->query_text;
			
			if (!empty($this->query_params)) {
				$_query_text = $this->prepareQueryText();
			}
		}
		
		$mysqli_result = $this->mysqli_instance->query($_query_text);
		
		if ($mysqli_result instanceof mysqli_result) {
			$result = new DatabaseMySqliResult($mysqli_result);
		} elseif ($mysqli_result == true) {
			$result = true;
		} else {
			throw new \Exception(str_replace('%NAME%', $_query_text, MSG_DATABASE_QUR) . ' ' . $this->mysqli_instance->error);
		}
		
		return $result;
	}
	
	/**
	 * Checks table for exists.
	 * 
	 * @param string $name Name of table.
	 * @return bool Returns true if table exists, otherwise returns false.
	*/
	public function tableExists($name)
	{
		$result	= true;
		$tmp	= $this->query_text;
		
		$this->setQueryText('SELECT 1 FROM ' . $name);
		
		/**
		 * try-catch block is used here because method executeQuery
		 * will throw exception when query was finished with errors.
		 * Assuming this - table which not exist will force to raise
		 * this exception.
		 */
		try
		{
			$this->executeQuery();
		}
		catch (\Exception $e)
		{
			$result = false;
		}
		/* Restore original query text. */
		$this->query_text = $tmp;
		
		return $result;
	}
	
	public function isConnected()
	{
		return $this->connected;
	}
	
	private function prepareQueryText()
	{
		$ld = '%';	$rd = '%';
		$query_text = $this->query_text;
		
		foreach ($this->query_params as $param => $value) {
			$prepared = Filter::sanitize($value);
			
			$query_text = str_replace($ld . $param . $rd, $prepared, $query_text);
		}
		
		return $query_text;
	}
}

}

?>
