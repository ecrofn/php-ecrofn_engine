<?php

namespace Ecrofn\Database\Mysql
{

use Ecrofn\Database\AbstractDatabaseResult;

use Ecrofn\Database\Mysql\TypeCaster;

class DatabaseMySqliResult extends AbstractDatabaseResult
{
	const RESULT_ASSOC	= 2;
	const RESULT_OBJECT	= 4;
	
	private $mysqli_result	= null;
	private $mysqli_caster	= null;
	
	public function __construct(\mysqli_result $mysqli_result)
	{
		$this->mysqli_result = $mysqli_result;
	}
	
	public function __destruct()
	{
		$this->free();
	}
	
	public function isEmpty()
	{
		return ($this->mysqli_result->num_rows == 0);
	}
	
	// Unloads query result.
	public function unload($variant = self::RESULT_ASSOC)
	{
		if ($this->mysqli_caster == null) {
			$this->mysqli_caster = new TypeCaster();
			
			$this->mysqli_caster->loadCastMap(
				$this->fillCastMap($this->mysqli_result->fetch_assoc())
			);
			$this->reset();
		}
		
		$data = array();
		if ($variant != self::RESULT_ASSOC) {
			$data = $this->unloadObject();
		} else {
			$data = $this->unloadAssoc();
		}
		$this->reset();
		
		return $data;
	}
	
	public function unloadColumns()
	{
		$columns = array();
		
		while ($column = $this->mysqli_result->fetch_field()) {
			$columns[] = $column->name;
		}
		$this->reset();
		
		return $columns;
	}
	
	private function unloadAssoc()
	{
		$data = array();
		
		while ($obj = $this->mysqli_result->fetch_assoc()) {
			$data[] = $this->mysqli_caster->cast($obj);
		}
		return $data;
	}
	
	private function unloadObject()
	{
		$data = array();
		
		while ($obj = $this->mysqli_result->fetch_object()) {
			$data[] = $this->mysqli_caster->cast($obj);
		}
		return $data;
	}
	
	private function free()
	{
		$this->mysqli_result->free();
		$this->mysqli_result = null;
	}
	
	// Сбрасывает указатель результата в начало, таким образом следующий
	// вызов получающих методов даст такой же результат, как и вызов
	// первого метода.
	private function reset()
	{
		$this->mysqli_result->field_seek(0);
		$this->mysqli_result->data_seek(0);
	}
	
	private function fillCastMap($node)
	{
		$length		= count($node);
		$cast_map	= array();
		
		for ($counter = 0; $counter < $length; $counter++) {
			$meta = $this->mysqli_result->fetch_field_direct($counter);
			
			$cast_map[$counter]['name'] = $meta->name;
			$cast_map[$counter]['type'] = $meta->type;
		}
		
		return $cast_map;
	}
}

}

?>