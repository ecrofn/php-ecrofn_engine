<?php

namespace Ecrofn\Database\Mysql
{

class TypeCaster
{
	private $source_result	= array();
	private $cast_map		= array();
	
	public function loadCastMap(array $data)
	{
		$this->cast_map = $data;
	}
	
	public function cast(array $data)
	{
		$length = count($data);
		
		for ($i = 0; $i < $length; $i++) {
			$type = $this->cast_map[$i]['type'];
			$node = &$data[$this->cast_map[$i]['name']];
			
			switch ($type) {
				case MYSQLI_TYPE_BIT		:
					$node = $this->castToBool($node);
					break;
				
				case MYSQLI_TYPE_TINY		:
				case MYSQLI_TYPE_SHORT		:
				case MYSQLI_TYPE_LONG		:
				case MYSQLI_TYPE_INT24		:
				case MYSQLI_TYPE_LONGLONG	:
					$node = $this->castToInteger($node);
					break;
				
				case MYSQLI_TYPE_FLOAT		:
				case MYSQLI_TYPE_DOUBLE		:
					$node = $this->castToFloat($node);
					break;
				
				case MYSQLI_TYPE_NULL		:
					$node = $this->castToNULL();
					break;
				
				default:
					$node = $this->castToString($node);
					break;
			}
		}
		
		return $data;
	}
	
	private function castToBool($value)
	{
		return (bool)$value;
	}
	
	private function castToInteger($value)
	{
		return (int)$value;
	}
	
	private function castToFloat($value)
	{
		return (float)$value;
	}
	
	private function castToString($value)
	{
		return (string)$value;
	}
	
	private function castToNULL()
	{
		return null;
	}
}

}

?>