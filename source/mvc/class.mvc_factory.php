<?php

namespace Ecrofn\Mvc
{

use Ecrofn\Mvc\Exception;
use Ecrofn\Di\ContainerInterface;

class MvcFactory
{
	const PARENT_NS_PATH			= '\Ecrofn\Mvc\\';
	const PARENT_CONTROLLER_CLASS	= 'AbstractController';
	const PARENT_MODEL_CLASS		= 'AbstractModel';
	const PARENT_VIEW_CLASS			= 'AbstractView';
	
	/**
	 * Creates the required MVC object.
	 * 
	 * @param type $class
	 * @param type $class_type
	 * @param ContainerInterface $dic
	 * @return \Ecrofn\Mvc\child_class	
	 * @throws \Ecrofn\System\Exception
	 */
	public static function create($class, $class_type, ContainerInterface $dic = null)
	{
		$obj = null;
		
		if (self::isClassTypeValid($class_type)) {
			if (class_exists($class)) {
				$base_class = self::PARENT_NS_PATH . $class_type;
				
				if (is_subclass_of($class, $base_class)) {
					/** @var \Ecrofn\Mvc\AbstractMvc */
					$obj = new $class();
					if ($dic != null) {
						$obj->setDic($dic);
					}
				} else {
					throw new Exception(
						"Class $class not extended from base class type $class_type."
					);
				}
			} else {
				throw new Exception(
					"Class $class not exist. Check class name and directory path."
				);
			}
		} else {
			throw new Exception("Unknown class type $class_type.");
		}
		
		return $obj;
	}
	
	private static function isClassTypeValid($class_type)
	{
		return
			$class_type == self::PARENT_CONTROLLER_CLASS	||
			$class_type == self::PARENT_MODEL_CLASS			||
			$class_type == self::PARENT_VIEW_CLASS			;
	}
}

}

?>