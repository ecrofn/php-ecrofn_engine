<?php

/** @todo 04/07/2016 Add to __init() method return type, analog of C main() function. */
/** @todo 04/07/2016 Add normalized comment to __init() method . */

namespace Ecrofn\Mvc
{

use Ecrofn\Mvc\AbstractMvc;
use Ecrofn\Mvc\MvcFactory;
use Ecrofn\System\Structure\Struct;

abstract class AbstractController extends AbstractMvc
{
	/** @var Ecrofn\Mvc\Model Current active model. */
	protected $model;
	/** @var Ecrofn\Mvc\View Current active view. */
	protected $view;
	
	/** * @var mixed Invoked method result. */
	private $method_result = null;
	
	private $models	= array();
	private $views	= array();
	
	private $_was_init = false;
	
	public function __construct() {}
	public function __destruct() {}
	
	/**
	 * Emulates standart __construct method, which allows to init all MVC
	 * objects in it's own classes, instead of creating it in
	 * MVC factory by using new() operator. Hower __construct method still
	 * remains, and could be useful for filling some constant data.
	 * 
	 * Please note, that this method automatically invokes when using factory
	 * class (MVC factory), to spawn required classes (controllers).
	 * 
	 * @param \Ecrofn\System\Structure\Struct $params List of parameters to constructor.
	 *
	 *  @return boolean If returned value is FALSE the application.
	 */
	protected function __init(Struct $params = null)
	{
		return true;
	}
	
	/**
	 * The default method, which will be invoked if no method name
	 * was set during dispatching process.
	 * 
	 * @param Struct $params List of required parameters.
	 */
	protected function __index(Struct $params = null)
	{
		return;
	}
	
	/**
	 * Invokes specific method and writes method result. Method result could be
	 * obtained later by using <b>getMethodResult</b> method. By using this
	 * method is only able to invoke public/protected methods.
	 * 
	 * Calling other methods directly by it name will give same result like
	 * this method does.
	 * 
	 * @param NULL|string $name Name of method being called. If param is empty,
	 * standart <b>__init</b> method will be called.
	 * @param \Ecrofn\System\Structure\Struct $params [optional]
	 * List of required parameters.
	 * 
	 * @return boolean TRUE if method was invoked, FALSE otherwise.
	 */
	public final function invokeMethod($name = '', Struct $params = null)
	{
		$result = true;
		/*
		* По соглашениям движка каждый вызываемый метод наследника (из внешнего
		* источника, например браузера) дополняется
		* единственным параметром - $params, который представляет из себя
		* структуру с параметрами для метода. Если вызываемый метод не оперирует
		* никакими параметрами, то будет создан пустой экземпляр структуры.
		*/
		if (!$params instanceof Struct)
		{
			$params = new Struct();
		}
		
		/* If no method name was passed, then init our defined constructor. */
		if (empty($name))
		{
			$name = AbstractMvc::ACTION_NAME;
		}
		
		/* Invoke required method. */
		if (method_exists($this, $name))
		{
			// C main() method analog should be implemented here.
			
			/** @todo 11/10/2016 After fixing stuff with __init function, the
			 * next code block would not be necessary.
			 */
			
			$status = true;
			
			if (!$this->_was_init) {
				$status = $this->__init($params);
				$this->_was_init = true;
			}
			
			$this->method_result = static::$name($params);
		}
		else
		{
			echo "METHOD $name OF CONTROLLER NOT EXIST";
			$result = false;
		}
		
		return $result;
	}
	
	public function getMethodResult()
	{
		return $this->method_result;
	}
	
	/**
	 * Initializes new \Ecrofn\Mvc\Controller model object.
	 * 
	 * @param string $class Class name.
	 * @param \Ecrofn\System\Structure\Struct $params [optional]
	 * 
	 * @return \Ecrofn\Mvc\Controller Instance of \Ecrofn\Mvc\Controller.
	 */
	protected final function initializeController($class, Struct $params = null)
	{
		$obj = MvcFactory::create(
			$class, MvcFactory::PARENT_CONTROLLER_CLASS
		);
		
		/** @todo 10/10/2016 Currently commented because it causing problems with
		 * some classes (Session), which are wrappers on standart PHP functions.
		 * Trying to instance from one controller another controller will gave
		 * this error (if they both are using Session).
		 * 
		 * Solution?...=\
		 */
		//$obj->__init($params);
		
		return $obj;
	}
	
	/**
	 * Initializes new \Ecrofn\Mvc\Model object. If object was already
	 * initialized, method is destroying it and makes new initialization.
	 * 
	 * @param string $class Class name.
	 */
	protected final function initializeModel($class)
	{
		/** @todo 11/10/2016 WTF does this block means? :p Posted an a half
		 * year ago, and i dont remember any damn thing about this thing =\...
		 */
		
		/* Forbid to call destructor of active model, if we have bunch of them. */
		if (count($this->models) == 0) {
			unset($this->model);
		}
		
		$this->model = MvcFactory::create($class, MvcFactory::PARENT_MODEL_CLASS);
	}
	
	/**
	 * Initializes new \Ecrofn\Mvc\View object. If object was already
	 * initialized, method is destroying it and makes new initialization.
	 * 
	 * @param string $class Class name.
	 */
	protected final function initializeView($class)
	{
		/* Forbid to call destructor of active view, if we have bunch of them. */
		if (count($this->views) == 0) {
			unset($this->view);
		}
		
		$this->view = MvcFactory::create($class, MvcFactory::PARENT_VIEW_CLASS);
	}
	
	/**
	 * Adds new model to the list. Changes active model to the
	 * currently added model.
	 * 
	 * @param string $name Alias of added model.
	 * @param string $class Class name.
	 */
	protected function addModel($name, $class)
	{
		$this->initializeModel($class);
		$this->models[$name] = $this->model;
	}
	
	/**
	 * Adds new view to the list. Changes active view to the
	 * currently added view.
	 * 
	 * @param string $name Alias of added view.
	 * @param string $class Class name.
	 */
	protected function addView($name, $class)
	{
		$this->initializeView($class);
		$this->views[$name] = $this->view;
	}
	
	/**
	 * Sets active model from available list.
	 * 
	 * @param string $name Name of model, which was added
	 * using addModel method.
	 */
	protected function setActiveModel($name)
	{
		if (isset($this->models[$name])) {
			$this->model = $this->models[$name];
		}
	}
	
	/**
	 * Sets active view from available list.
	 * 
	 * @param string $name Name of view, which was added
	 * using addView method.
	 */
	protected function setActiveView($name)
	{
		if (isset($this->views[$name])) {
			$this->view = $this->views[$name];
		}
	}
	
	protected function getActiveModel()
	{
		return $this->model;
	}
	
	protected function getActiveView()
	{
		return $this->view;
	}
	
	protected function getModel($name)
	{
		if (isset($this->models[$name])) {
			return $this->models[$name];
		}
	}
	
	protected function getView($name)
	{
		if (isset($this->views[$name])) {
			return $this->views[$name];
		}
	}
}

}

?>