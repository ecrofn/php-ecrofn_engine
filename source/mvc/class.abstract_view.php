<?php

namespace Ecrofn\Mvc
{

use Ecrofn\Mvc\AbstractMvc;
use Ecrofn\Mvc\MvcFactory;
use Ecrofn\System\Structure\Struct;

abstract class AbstractView extends AbstractMvc
{	
	/** @var string Path to templates directory. */
	protected $templates_path	= '';
	
	/** @var mixed Invoked method result. */
	protected $method_result	= null;
	
	/** @var \Ecrofn\System\Structure\Struct Path list to all renderable objects. */
	protected $renderer_list	= null;
	
	/** @var \Ecrofn\System\Structure\Struct Data, which will be used during rendering process. */
	protected $renderer_data	= null;
	
	/** @var int Count of rendered objects. */
	private $rendered_objects	= 0;
	
	public function __construct()
	{
		/**
		 * renderer_top
		 * renderer
		 * renderer_bottom
		 */
		$this->renderer_list = new Struct(
			'renderer_t, renderer, renderer_b'
		);
		$this->renderer_list->setCommonKeysValue(array());
		
		/**
		 * block_data
		 * variable_data
		 */
		$this->renderer_data = new Struct(
			'block_data, variable_data'
		);
		$this->renderer_data->setCommonKeysValue(array());
	}
	
	public function __destruct() {}
	
	/**
	 * Sets templates directory path. All included templates are obtained
	 * from this directory path. Calling this method without argument will
	 * resets templates root directory.
	 * 
	 * @param string $root Templates directory path.
	 */
	public final function setTemplatesRoot($root = '')
	{
		$this->templates_path = $root;
	}
	
	/**
	 * Invokes specific method and writes method result. Method result could be
	 * obtained later by using <b>getMethodResult</b> method. By using this
	 * method is only able to invoke public/protected methods.
	 * 
	 * Calling other methods directly by it name will give same result like
	 * this method does.
	 * 
	 * @param string $name Name of method being called.
	 * @param \Ecrofn\System\Structure\Struct $params [optional]
	 * List of required parameters.
	 * 
	 * @return boolean TRUE if method was invoked, FALSE otherwise.
	 */
	public final function invokeMethod($name, Struct $params = null)
	{
		$result = true;
		/*
		* По соглашениям движка каждый вызываемый метод наследника дополняется
		* единственным параметром - $params, который представляет из себя
		* структуру с параметрами для метода. Если вызываемый метод не оперирует
		* никакими параметрами, то будет создан пустой экземпляр структуры.    
		*/
		if (!$params instanceof Struct)
		{
			$params = new Struct();
		}
		
		if (method_exists($this, $name))
		{
			$this->setMethodResult(static::$name($params));
		}
		else
		{
			echo "METHOD $name OF VIEW NOT EXIST";
			$result = false;
		}
		
		return $result;
	}	
	
	public function getMethodResult()
	{
		
	}
	
	/**
	 * Adds template to renderer list.
	 * 
	 * @param string $path
	 * @param bool $path_dependent
	 */
	public function addToList($path, $path_dependent = true)
	{
		if ($path_dependent)
		{
			$this->renderer_list->renderer[] = $this->templates_path . $path;
		}
		else
		{
			$this->renderer_list->renderer[] = $path;
		}
		
		/* Код описанный выше равносилен коду описанному ниже, но без
		 * магических символов.
		 * $this->renderer_list[count($this->renderer_list)] = $path;
		 */
	}
	
	/**
	 * Adds template to top renderer list.
	 * 
	 * @param string $path
	 * @param bool $path_dependent
	 */
	public function addToHeaderList($path, $path_dependent = true)
	{
		if ($path_dependent)
		{
			$this->renderer_list->renderer_t[] = $this->templates_path . $path;
		}
		else
		{
			$this->renderer_list->renderer_t[] = $path;
		}
	}
	
	/**
	 * Adds template to bottom renderer list.
	 * 
	 * @param string $path
	 * @param bool $path_dependent
	 */
	public function addToFooterList($path, $path_dependent = true)
	{
		if ($path_dependent)
		{
			$this->renderer_list->renderer_b[] = $this->templates_path . $path;
		}
		else
		{
			$this->renderer_list->renderer_b[] = $path;
		}
	}
	
	/**
	 * Starts rendering process.
	 * 
	 * @param string|null $path Path to renderable object. If empty, then
	 * starts rendering of all objects, which were added using addToList method.
	 * @param bool $path_dependent
	 */
	public final function render($path = null, $path_dependent = true)
	{		
		if (!$path == null)
		{
			if ($path_dependent)
			{
				$path = $this->templates_path . $path;
			}
			
			//$this->renderer->render($path);
			$this->renderTemplate($path);
		}
		else
		{
			ob_start();
			foreach ($this->renderer_list as $value)
			{
				$length = count($value);
				
				for ($counter = 0; $counter < $length; $counter++)
				{
					//$this->renderer->render($value[$counter]);
					$this->renderTemplate($value[$counter]);
				}
			}
			ob_end_flush();
		}
	}
	
	/**
	 * Sets up required data for renderer process. All data which were retrived
	 * in parameters - mergers, and saves with selected alias name. If count of
	 * parameters are bigger than one, all data represents as indexed array,
	 * otherwise it will be converted to string (except array it self).
	 * 
	 * @param string $name Alias (Variable name) of installed data.
	 * @param mixed $data Data, to be installed.
	 * @param mixed $dataN DataN, to be installed.
	 */
	public final function assign($name, $data, $dataN = null)
	{
		$params = func_get_args();
		/* Remove 1st parameter - $name. */
		array_shift($params);
		
		$array_cast = count($params) > 1 ? true : false;
		
		foreach ($params as $key => $value)
		{
			if ($array_cast)
			{
				$this->renderer_data->variable_data[$name][] = $value;
			}
			else
			{
				$this->renderer_data->variable_data[$name] = $value;
			}
		}
	}
	
	public function getRenderedObjectsCount()
	{
		return $this->rendered_objects;
	}
	
	protected function setMethodResult($result)
	{
		
	}
	
	/**
	 * Initializes new \Ecrofn\Mvc\View view object.
	 * 
	 * @param string $class Class name.
	 * @return \Ecrofn\Mvc\View Instance of \Ecrofn\Mvc\View object.
	 */
	protected final function initializeView($class)
	{
		return MvcFactory::create(
			$class, MvcFactory::PARENT_VIEW_CLASS
		);
	}
	
	/**
	 * Outputs file contents to the output stream.
	 * 
	 * @param string $path Path to file.
	 */
	private function renderTemplate($path)
	{
		if (file_exists($path))
		{
			include $path;
			$this->rendered_objects++;
		}
		else
		{
			echo "<br />Template '$path' not exist, replace it correctly!";
		}
	}
	
	
	/* NEXT METHODS LIST USED DURING TEMPLATE RENDERING PROCESS. */
	
	protected final function echoString($data)
	{
		/* Casting anonymous function. */
		$convert_rules = function(&$value) {
			$result	= null;
			$type	= gettype($value);
			
			switch ($type)
			{
				case 'boolean'	:
				case 'integer'	:
				case 'double'	:
				case 'string'	:
					$result = (string) $value;
					break;
				case
					'array'		:
					$result = $value;
					break;
				default:
					$result = 'TYPE CONVERT ERROR';
					break;
			}
			
			return $result;
		};
		
		if (is_array($data))
		{
			echo 'ARRAY DETECTED. USE LOOP COMMAND TO SHOW IT ELEMENTS';
		}
		else
		{
			echo $convert_rules($data);
		}
	}
	
	
	public function blockBegin($name)
	{
		$this->renderer_data->block_data[$name] = '';
		ob_start();
	}
	
	public function blockEnd($name)
	{
		$data = ob_get_clean();
		$this->renderer_data->block_data[$name] = $data;
	}
}

}

?>