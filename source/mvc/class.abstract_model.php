<?php

namespace Ecrofn\Mvc
{

use Ecrofn\Mvc\AbstractMvc;
use Ecrofn\Mvc\MvcFactory;

use Ecrofn\System\Structure\Struct;

abstract class AbstractModel extends AbstractMvc
{
	/** * @var mixed Invoked method result. */
	protected $method_result = null;
	
	public function __construct() {}
	public function __destruct() {}
	
	/**
	 * Invokes specific method and writes method result. Method result could be
	 * obtained later by using <b>getMethodResult</b> method. By using this
	 * method is only able to invoke public/protected methods.
	 * 
	 * Calling method directly by it name will give same result like this
	 * method does.
	 * 
	 * @param string $name Name of method being called.
	 * @param \Ecrofn\System\Structure\Struct $params [optional]
	 * The list of the required parameters.
	 * 
	 * @return boolean TRUE if method was invoked, FALSE otherwise.
	 */
	public final function invokeMethod($name, Struct $params = null)
	{
		$result = true;
		/*
		* По соглашениям движка каждый вызываемый метод наследника дополняется
		* единственным параметром - $params, который представляет из себя
		* структуру с параметрами для метода. Если вызываемый метод не оперирует
		* никакими параметрами, то будет создан пустой экземпляр структуры.    
		*/
		if (!$params instanceof Struct)
		{
			$params = new Struct();
		}
		
		if (method_exists($this, $name))
		{
			$this->setMethodResult(static::$name($params));
		}
		else
		{
			echo "METHOD $name OF MODEL NOT EXIST";
			$result = false;
		}
		
		//$this->checkMethodReturnValue($result);
		//$this->setMethodResult($result);
		
		return $result;
	}
	
	public final function getMethodResult()
	{
		return $this->method_result;
	}
	
	protected function setMethodResult($result)
	{
		$this->method_result = $result;
 	}
	
	/**
	 * Initializes new \Ecrofn\Mvc\Model model object.
	 * 
	 * @param string $class Class name.
	 * @return \Ecrofn\Mvc\Model Instance of \Ecrofn\Mvc\Model object.
	 */
	protected final function initializeModel($class)
	{
		return MvcFactory::create(
			$class, MvcFactory::PARENT_MODEL_CLASS
		);
	}
}

}

?>