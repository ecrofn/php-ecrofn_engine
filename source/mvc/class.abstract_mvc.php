<?php

namespace Ecrofn\Mvc
{

use Ecrofn\Mvc\MvcInterface;

use Ecrofn\Di\ContainerInjectInterface;
use Ecrofn\Di\ContainerInterface;

use Ecrofn\System\Structure\Struct;

/**
 * Base MVC class.
 */
abstract class AbstractMvc implements MvcInterface, ContainerInjectInterface
{
	const CONSTRUCTOR_NAME	= '__init';
	const ACTION_NAME		= '__index';
	
	protected $dic;
	
	/**
	 * Sets the dependency injector container.
	 * 
	 * @param \Ecrofn\Di\Container $container
	 */
	public function setDic(ContainerInterface $container)
	{
		$this->dic = $container;
	}
	
	/**
	 * Gets the internal dependency injector container.
	 */
	public function getDic()
	{
		return $this->dic;
	}
	
	public abstract function invokeMethod($name, Struct $params = null);
	
	public abstract function getMethodResult();
}

}

?>
