<?php

namespace Ecrofn\Mvc
{

interface MvcInterface
{
	/**
	* Данный метод представляет из себя единую точку входа для вызова методов
	* из дочерних классов. При вызове этого метода из дочернего класса
	* модификаторы методов дочернего класса должны быть объявлены как protected.
	* (объявление через модификатор private вызовет ошибку доступа, т.к. доступ
	* не работает, по причине того, что в дочерних классах не переопределен
	* текущий метод).
	* Используя механизм LSB мы точно уверены в том, что будут вызваны именно
	* дочерние методы, а не его родителя.
	*
	* Подробнее:
	* http://php.net/manual/ru/language.oop5.late-static-bindings.php
	* http://stackoverflow.com/questions/12934744/calling-a-child-method-from-the-parent-class-in-php
	*/	
	public function invokeMethod($name, \Ecrofn\System\Structure\Struct $params = null);
	
	public function getMethodResult();
}

}

?>