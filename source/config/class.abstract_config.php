<?php

namespace Ecrofn\Config
{

use Ecrofn\Config\ConfigInterface;
use Ecrofn\Config\Exception;
use Ecrofn\System\FileSystem\File;

class AbstractConfig implements ConfigInterface
{
	const MODE_JSON				= 2;
	const MODE_SERIALIZED		= 4;
	const MODE_STRICT			= 8;
	
	protected static $config	= array();
	protected static $keys		= array();
	
	protected $name				= 'config';
	protected $full_access		= false;
	
	private $delimeter			= '.';
	private $prefix_name		= '';
	
	public function __construct($path = '')
	{
		if (!empty($path)) {
			// Load config from file.
		}
	}
	
	/**
	 * 
	 * 
	 * @param string $name
	 * @param mixed $value
	 * @throws \Ecrofn\System\Exception
	 */
	public function set($name, $value)
	{
		$splitted_name = $this->splitRelativeKeyName($this->prefix_name . $name);
		
		$chain	= $splitted_name['chain'];
		$key	= $splitted_name['key'];
		
		/* The reference to some node of $config array. */
		$node = null;
		
		/**
		 * When chain is empty, that means that we are trying to write directly
		 * to the root element ($config) without any groups. This is probably
		 * caused by using full access mode.
		 * Checking for reference existence is not necessary in this case,
		 * as we already have it (root element, $config).
		 */
		if (empty($chain)) {
			$node =& self::$config;
		}
		else {
			/* When chain reference not exists - we need to create it. */
			if (!$this->isReferenceExist($chain)) {
				$crumbs	= explode($this->delimeter, $chain);
				
				$index = 0;
				
				foreach ($crumbs as $crumb) {
					if (isset($node)) {
						$node =& $this->getNestedElement($node, $crumb);
					} else {
						$node =& $this->getNestedElement(self::$config, $crumb);
					}
					
					$index++;
					
					/**
					 * If current node is having scalar type value (int, float,
					 * string, bool) instead of array type value - halt the
					 * execution immediately.
					 * 
					 * Example:
					 * $config->set('foo.bar', 2); // OK, key has been created.
					 * $config->set('foo.bar.baz', 4) // Error, because nested
					 *	// element 'bar' already created and contains scalar
					 *	// value. So method handler couldn't dive to the next
					 *	// nested element 'baz' because 'bar' is scalar type
					 *	// value.
					 * 
					 * And another one:
					 * $config->set('foo.bar', array()); // OK, key has been created.
					 * $config->set('foo.bar.baz', 4) // OK, key has been created,
					 *	// because now 'bar' is non scalar, and method handler
					 *	// able to dive in and create new element.
					 */
					if (!is_array($node)) {
						$msg = '';
						for ($counter = 0; $counter < $index - 1; $counter++) {
							$msg .= $crumbs[$counter] . $this->delimeter;
						}
						$msg .= $crumbs[$counter];
						
						throw new Exception("Collision detected! Cannot get next nested element from scalar key $msg!");
					}
				}
				/* Save direct link to this chain. */
				self::$keys[$chain] =& $node;
			}
			/* Or use the already created reference. */
			else {
				$node =& self::$keys[$chain];
			}
		}
		/**
		 * Finally, assign to selected node some value and destroy reference to
		 * this node.
		 */
		$node[$key] = $value;
		unset($node);
	}
	
	/**
	 * Gets value, which corresponds selected key.
	 * 
	 * @param string $name The name of the key.
	 * @return mixed The value, which corresponds selected key.
	 * @throws \Ecrofn\System\Exception When key does not exists.
	 */
	public function get($name)
	{
		$splitted_name = $this->splitRelativeKeyName($this->prefix_name . $name);
		
		$chain	= $splitted_name['chain'];
		$key	= $splitted_name['key'];
		
		if ($this->isReferenceExist($chain)) {
			if (isset(self::$keys[$chain][$key])) {
				return self::$keys[$chain][$key];
			}
		}
		
		throw new Exception("Key $key from chain $chain not exist!");
	}
	
	/**
	 * 
	 * @param type $data
	 */
	public function load(array $data)
	{
		$this->setRecursive($data);
	}
	
	/**
	 * 
	 * @param type $path
	 * @param type $mode
	 * @throws Exception
	 */
	public function loadFromFile($path, $mode = self::MODE_JSON)
	{
		$file		= new File($path);
		$content	= $file->getContent();
		$data		= null;
		
		if ($mode == self::MODE_SERIALIZED) {
			$data = unserialize($content);
		} else if ($mode == self::MODE_SERIALIZED) {
			$data = $content;
		} else {
			$data = json_decode($content, true);
		}
		
		if (!is_array($data)) {
			throw new Exception("Downloadble data should have an array type!");
		}
		
		$this->load($data);
	}
	
	/**
	 * Dives in the selected array. If array does not have objects to dive,
	 * new one will be created, otherwise will be returned existing.
	 * 
	 * @param array $obj The array for dive in.
	 * @param string $key They key to be checked.
	 * @return array The array, which was returned during diving process.
	 */
	protected function &getNestedElement(array &$obj, $key)
	{
		if (!isset($obj[$key])) {
			$obj[$key] = array();
		}
		return $obj[$key];
	}
	
	/**
	 * Checks reference for existence.
	 * 
	 * @param string $key_name The reference name to be checked.
	 * @return bool TRUE if reference exist, FALSE if not.
	 */
	protected function isReferenceExist($key_name)
	{
		return isset(self::$keys[$key_name]) ? true : false;
	}
	
	/**
	 * 
	 * @param type $full_access
	 */
	protected function setFullAccess($full_access)
	{
		$this->full_access = $full_access;
		$this->generatePrefixName();
	}
	
	/**
	 * 
	 * @param type $name
	 * @throws Exception
	 */
	protected function setName($name)
	{
		if (empty($name)) {
			throw new Exception("Config name should not be empty!");
		}
		
		$this->name = $name;
		$this->generatePrefixName();
	}
	
	/**
	 * Recursively sets elements in container.
	 * Alternative of <b>set</b> method, but using loop way.
	 * 
	 * @param array $data The associative array, which need to be loaded.
	 * @param string $prefix Prefix, which represents full path to current
	 * array from root array.
	 */
	protected function setRecursive(array &$data, $prefix = '')
	{
		foreach ($data as $element => $value) {
			
			$element = (string)$element;
			
			if (is_array($value)) {
				if (!empty($prefix)) {
					$this->setRecursive($value, $prefix . $this->delimeter . $element);
				} else {
					$this->setRecursive($value, $element);
				}
			} else {
				if (!empty($prefix)) {
					$this->set($prefix . $this->delimeter . $element, $value);
				} else {
					$this->set($element, $value);
				}
			}
		}
	}
	
	/**
	 * 
	 */
	protected function generatePrefixName()
	{
		if ($this->full_access) {
			$this->prefix_name = '';
		} else {
			$this->prefix_name = $this->name . $this->delimeter;
		}
	}
	
	/**
	 * Splits input value by chain & key values.
	 * 
	 * @param string $name The name to be splitted.
	 * @return array An array of splitted strings.
	 */
	protected function splitRelativeKeyName($name)
	{
		$result = array(
			'chain'	=> '',
			'key'	=> $name
		);
		
		$pos = strrpos($name, $this->delimeter);
		
		if ($pos !== false) {
			$result['chain']	= substr($name, 0, $pos);
			$result['key']		= substr($name, $pos + 1);
		}
		
		return $result;
	}
}

}

?>