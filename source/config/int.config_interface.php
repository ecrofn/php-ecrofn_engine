<?php

namespace Ecrofn\Config
{

interface ConfigInterface
{
	public function set($name, $value);
	
	public function get($name);
}

}

?>