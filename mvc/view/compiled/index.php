<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Engine notification page</title>
<link href="<?php $this->echoString(D_ENGINE_PUBLIC_WEB); ?>styles/style.css" rel="stylesheet">
</head>
<body>
<div class="main">
	<div class="logo"><a href="http://www.ecrofn.ru"><img src="<?php $this->echoString(D_ENGINE_ROOT_WEB); ?>public/images/engine_logo.png" /></a></div>
	<div class="block">
	<div class="block_ins">
		<h1>Engine notification block:</h1>
		<div class="text"><?php if (!empty($this->renderer_data->variable_data['msg'])) { $this->echoString($this->renderer_data->variable_data['msg']); } else { echo('EMPTY VARIABLE VALUE'); } ?></div>
		<div class="footer">
			<a href="http://www.ecrofn.ru">EcrofnEngine</a>
			<br />
			<br />2013-2016
		</div>
	</div>
	</div>
</div>
</body>
</html>