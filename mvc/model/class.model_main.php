<?php

namespace EcrofnDemo\Mvc\Model
{

class ModelMain extends \Ecrofn\Mvc\AbstractModel
{	
	public function __construct() {}
	
	public function __destruct() {}
	
	protected function prepare_error_text(\Ecrofn\System\Structure\Struct $params)
	{
		//return array('msg' => 'WARNING : ' . $params->msg);
		return 'WARNING : ' . $params->msg;
	}
}

}

?>