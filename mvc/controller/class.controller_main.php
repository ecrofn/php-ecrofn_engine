<?php

namespace EcrofnDemo\Mvc\Controller
{

class ControllerMain extends \Ecrofn\Mvc\AbstractController
{	
	public function __construct()
	{
		$this->initializeModel('\EcrofnDemo\Mvc\Model\ModelMain');
		$this->initializeView('\EcrofnDemo\Mvc\View\ViewMain');
		
		$this->view->setTemplatesRoot(D_ENGINE_ROOT . 'mvc/view/compiled/');
	}
	
	public function __destruct() {}
	
	// Отладочная заготовка.
	protected function index(\Ecrofn\System\Structure\Struct $params)
	{		
		$this->view->addToList('index.php');
		$this->view->render();
	}
	
	// Отсутствует требуемый модуль.
	protected function module_not_found(\Ecrofn\System\Structure\Struct $params)
	{
		$params->msg = str_replace('%NAME%', $params->msg, MSG_MODULE_NOT_FOUND);
		
		$this->model->invokeMethod('prepare_error_text', $params);
		
		$this->view->addToList('index.php');
		$this->view->assign($this->model->getMethodResult());
		$this->view->render();
	}
	
	// Отсутствует требуемый контроллер.
	protected function controller_not_found(\Ecrofn\System\Structure\Struct $params)
	{
		$params->msg = \str_replace('%NAME%', $params->msg, MSG_CONTROLLER_NOT_FOUND);
		
		$this->model->invokeMethod('prepare_error_text', $params);
		
		$this->view->addToList('index.php');
		$this->view->assign($this->model->getMethodResult());
		$this->view->render();
	}
	
	// Файл с путями отсутствует.
	protected function routes_not_found(\Ecrofn\System\Structure\Struct $params)
	{
		$params->msg = \str_replace('%NAME%', $params->msg, MSG_CONTROLLER_NOT_FOUND);
		
		$this->model->invokeMethod('prepare_error_text', $params);
		
		$this->view->addToList(D_ENGINE_VIEW . DIR_SEPARATOR . 'compiled' . DIR_SEPARATOR . 'index.php');
		$this->view->assign('main', array($this->model->getMethodResult()));
		$this->view->render();
	}
	
	// 404 ошибка.
	protected function route_not_found(\Ecrofn\System\Structure\Struct $params)
	{
		//$params->msg = MSG_404_NOT_FOUND;
		$params->msg = 'Some error occurred!';
		
		$this->model->invokeMethod('prepare_error_text', $params);		
		
		$this->view->addToList('index.php');
		$this->view->assign('msg', $this->model->getMethodResult());
		
		$this->view->render();
	}
}

}

?>