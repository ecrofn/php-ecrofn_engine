<?php

namespace EcrofnDemo\Mvc\Controller
{

use Ecrofn\Mvc\AbstractController;

use Ecrofn\System\FileSystem\Directory;
use Ecrofn\System\Structure\Struct;

use Ecrofn\Event\Event;
use Ecrofn\Event\EventManager;

use Ecrofn\Gui\Generator;

class ControllerGenerator extends AbstractController
{
	/** @var \Ecrofn\GUI\Generator Class instance. */
	private $instance = null;
	private $tabs_offset = null;
	
	public function __construct() {}
	
	public function __destruct() {}
	
	protected function __init(Struct $params = null)
	{
		$root = D_ENGINE_SOURCE . 'gui/generator/';
		
		$this->instance	= new Generator();
		$this->instance->loadConvertRules($root . 'inc.generator_rules.php');
		
		if ($params->has('engine')) {
			$source_path	= D_ENGINE_ROOT . 'mvc/view/sources/';
			$compiled_path	= D_ENGINE_ROOT . 'mvc/view/compiled/';
		} else {
			$source_path	= D_MODULE_SOURCE . 'mvc/view/sources/';
			$compiled_path	= D_MODULE_SOURCE . 'mvc/view/compiled/';
		}
		
		echo 'Execution started...<br />';
		
		if ($params->has('wipe')) {
			echo '<br />Wiping process started.<br />';
			$this->tabs_offset = 0;
			$this->cleanDirectory($compiled_path);
			echo '<br />Wiping process finished.<br />';
		}
		
		
		echo '<br />Template-compiling process started.';
		$this->tabs_offset = 0;
		$this->compileTemplates($source_path, $compiled_path);
		echo '<br />Template-compiling process finished.';
		
		echo '<br /><br />...Execution finished.';
	}
	
	/**
	 * Cleans ups selected directory from all files.
	 * All files will be deleted permanently!
	 * 
	 * @param string $path Path to folder, which will be cleaned up.
	 */
	private function cleanDirectory($path)
	{
		/* Add another 16 spaces. */
		$tabs16 = '<br />' . str_repeat('&nbsp;', $this->tabs_offset * 16);
		
		$e1 = new Event(
			'beforeDelete',
			function($obj) use ($tabs16) {
				echo $tabs16 . 'Preparing to delete ' .
					($obj->isDirectory() ? 'DIR ' : 'FILE ') .
					$obj->getPath();
			}
		);
		$e2 = new Event(
			'afterDelete',
			function($obj) use ($tabs16) {			
				echo $tabs16 . 'Completion of deleting ' .
					($obj->isDirectory() ? 'DIR ' : 'FILE ') .
					$obj->getPath();
			}
		);
		$em = new EventManager(array($e1, $e2));
		
		$dir = new Directory($path);
		$files = $dir->getFiles();
		
		foreach ($files as $file) {			
			$file->setEventsManager($em);
			
			$fpath = $file->getPath();
			
			if ($file->isDirectory()) {
				$this->tabs_offset++;
				
				$this->cleanDirectory($fpath);
				$file->delete();
				
				$this->tabs_offset--;
			} else {
				$file->delete();
			}
		}
	}
	
	/**
	 * Initializes template compilation process.
	 * 
	 * @param string $source_path Path to folder with source files.
	 * @param string $compiled_path Path to folder, where compiled files will
	 *	be put.
	 * @param array $ignored_files List of files which will be ignored
	 *	during clean up process.
	 */
	private function compileTemplates($source_path, $compiled_path)
	{
		$tabs16	= '<br />' . str_repeat('&nbsp;', $this->tabs_offset * 16);
		
		$dir = new Directory($source_path);
		$files = $dir->getFiles();
		
		foreach ($files as $file) {
			$source_file_path	= $file->getPath();
			$compiled_file_path	= $compiled_path . '/' . $file->getName();
			
			$fpath = $file->getPath();
			
			if ($file->isDirectory()) {
				if (!Directory::exists($compiled_file_path)) {
					echo $tabs16 . 'Creating dir \'' . $fpath . '\'...';
					new Directory($compiled_file_path, true);
				}
				$this->tabs_offset++;
				echo $tabs16 . 'Deeping in DIR \'' . $fpath . '\'...';
				$this->compileTemplates($source_file_path, $compiled_file_path);
				echo $tabs16 . 'Returning from DIR \'' . $fpath . '\'...';
				$this->tabs_offset--;
			} else {
				echo $tabs16 . 'Generating template \'' . $fpath . '\'...';
				$this->instance->generate($source_file_path, $compiled_file_path);
			}
		}
	}
}

}

?>